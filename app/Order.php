<?php

namespace App;

class Order extends Model
{
	public static $shipping_charge_min = 400;
	public static $shipping_percent = 10; // 10%

	public static $bdboom_charge_min = 200;
	public static $bdboom_percent = 5; // 5%

	public static $gateway_charge_percent = 2.8; // 2.5%

	public static $delivery_charge = 0; // 5%
	public static $discount = 0; // 5%

	public static $search_fields = ['id'];

	public function products(){
		return $this->hasMany(OrderProduct::class);
	}

	public function payment(){
		return $this->hasOne(Payment::class);
	} 

	public static function shipping_charge($price){
		$shipping_charge = (self::$shipping_percent / 100) * $price;
		if($shipping_charge < self::$shipping_charge_min){
			return self::$shipping_charge_min;
		}
		return $shipping_charge;
	}

	public static function bdboom_charge($price){
		$bdboom_charge = (self::$bdboom_percent / 100) * $price;
		if($bdboom_charge < self::$bdboom_charge_min){
			return self::$bdboom_charge_min;
		}
		return $bdboom_charge;
	}

	public static function get_gateway_charge($price){
		$gateway_charge = (self::$gateway_charge_percent / 100) * $price;
		return $gateway_charge;
	}

	public static function get_delivery_charge(){
		return self::$delivery_charge ? self::$delivery_charge : 0;
	}

	public static function get_discount(){
		return self::$discount ? self::$discount : 0;
	}

	public static function get_all_total($grand_total, $delivery_charge, $discount){
		return $grand_total + $delivery_charge - $discount;
	}

	public function product_list(){
		$product_list = [];
		foreach ($this->products as $product) {
			$item = array(
                'name' => $product->title,
                'price' => $product->total,
                'quantity' => $product->qty,
                );
            $product_list[] = $item;
		}
		// $product_1 = array('name' => 'Adata 16GB Pendrive','price' => 5,'quantity' => 2);
        // $product_2 = array('name' => 'Adata 8GB Pendrive','price' => 5,'quantity' => 1);
        // $products = array($product_1,$product_2);
		return $product_list;
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public static $payment_status = [
		'1' => 'Paid',
		'2' => 'Pending',
		'3' => 'Cancelled',
		'4' => 'Failed'
	];

	public static function get_payment_status_name_by_key($key){
		if(array_key_exists($key, static::$payment_status)){
			return static::$payment_status[$key];
		}
		return '';
	}

	public static $delivery_status = [
		'0' => 'Payment Pending',
		'1' => 'Payment Confirmed',
		'2' => 'Processing',
		'3' => 'Shipped from India',
		'4' => 'Arrived in BD',
		'5' => 'Deliver Processed',
		'6' => 'Delivered',
	];

	public static function get_delivery_status_name_by_key($key){
		if(array_key_exists($key, static::$delivery_status)){
			return static::$delivery_status[$key];
		}
		return '';
	}
	
	


	public function create_order(){
			$total_cart = \App\Cart::get_total_cart();
			// dd($total_cart);

			if(!empty($total_cart)){
	    		$order = 
		    		static::create([
		    			'user_id' => auth()->user()->id,
						'grand_qty' => $total_cart['grand_qty'],
						'grand_net_price' => $total_cart['grand_net_price'],
						'grand_shipping_charge' => $total_cart['grand_shipping_charge'],
						'grand_bdboom_charge' => $total_cart['grand_bdboom_charge'],
						'grand_total' => $total_cart['grand_total'],
						'discount' => $total_cart['discount'],
						'delivery_charge' => $total_cart['delivery_charge'],
						'all_total' => $total_cart['all_total'],
						'gateway_charge' => $total_cart['gateway_charge'],
						'master_total' => $total_cart['master_total'],

						'shipping_telephone' => request('shipping_telephone'),
						'shipping_city' => request('shipping_city'),
						'shipping_state' => request('shipping_state'),
						'shipping_postal_code' => request('shipping_postal_code'),
						'shipping_address' => request('shipping_address'),
						'shipping_country_id' => request('shipping_country_id'),

						'payment_status' => 2, // 2 pending
					]);
					//pr($order, true);
				return $order;
	    	}
	    	
	}

}
