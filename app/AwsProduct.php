<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

define("AMAZONAFFID", "airposted-21");
define("ACCESSKEYID", "AKIAJHL5WOYKTZCLZ25A");
define("AMAZONSECRETKEY", "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");
define("ENDPOINT", "webservices.amazon.in");
define("ENDPOINTSITE", "www.amazon.in");

class AwsProduct extends Model
{

 	public static function getItemSearch($keyword, $item_page=1)
    {
      
        $uri = "/onca/xml";
        $params = array(
            "Service" => "AWSECommerceService",
            "AWSAccessKeyId" => ACCESSKEYID,
            "AssociateTag" => AMAZONAFFID,
            "Operation" => "ItemSearch",
            "Keywords" => $keyword,
            "Condition" => "All",
            "Availability" => "Available",
            "SearchIndex" => "All",
            "MinimumPrice" => 1,
            //"Sort" => "price",
            "ResponseGroup" => "Images,ItemAttributes,Offers",
            "ItemPage" => $item_page
        );

        // Set current timestamp if not set
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }

        // Sort the parameters by key
        ksort($params);
        $pairs = array();
        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
        }
        // Generate the canonical query
        $canonical_query_string = join("&", $pairs);
       // Generate the string to be signed
        $string_to_sign = "GET\n".ENDPOINT."\n".$uri."\n".$canonical_query_string;
        // Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, AMAZONSECRETKEY, true));
        // Generate the signed URL
        return $request_url = 'https://'.ENDPOINT.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);       
    }



    public static function getAWSItemLookup($ItemId)
    {
       
        $uri = "/onca/xml";
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemLookup",
            "AWSAccessKeyId" => ACCESSKEYID,
            "AssociateTag" => AMAZONAFFID,
            // "SearchIndex" => "All",         
            "ItemId" => $ItemId,
            "ResponseGroup" => "OfferFull,Images,ItemAttributes,Offers"
         
        );

        // Set current timestamp if not set
        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }

        // Sort the parameters by key
        ksort($params);
        $pairs = array();
        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
        }

        // Generate the canonical query
        $canonical_query_string = join("&", $pairs);
        // Generate the string to be signed
        $string_to_sign = "GET\n".ENDPOINT."\n".$uri."\n".$canonical_query_string;
        // Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, AMAZONSECRETKEY, true));
        // Generate the signed URL
        return $request_url = 'https://'.ENDPOINT.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);      
    }


    public static function getAWSLink($ItemId)
    {        
        // Generate the signed URL
        return $request_url = 'https://'.ENDPOINTSITE.'/gp/product/'.$ItemId.'?tag='.AMAZONAFFID;
    }


    public static function getFormat($data){

    	if(is_array($data)){
    		foreach ($data as $key => $value) {
    			return $value;
    		}
    	}else{
    		return $data;
    	}
 	
    }


    public static function getUrlData($url){

    	if($url):
	    	preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si',  $url, $link);

	    	if(is_array($link) && count($link)):
	    		return self::getItemId($url);
	    	else:
	    		return 'itemSearch';
	    	endif;
	    else:
	    	return 'invalid';
	    endif;
    }

    public static function getItemId($url){
  	
    	$start = 0;
    	$lenght = 10;
    	$start = strpos($url, 'gp/product/')?strpos($url, '/gp/product/')+12:strpos($url, '/dp/')+4;

    	if($start && $start > 4){
    		$get_url = substr($url, $start,$lenght);
    	}else{
    		$get_url = 'invalid';
    	}

    	return $get_url;
    }


    public static function getPrice($data){
        if(isset($data->FormattedPrice)):
    	   return $data->FormattedPrice?$data->FormattedPrice:'';
        else:
            return '';
        endif;
    }

    public static function getOffers($data){
        if(is_array($data)):
        	foreach ($data as $key => $items) {
        		if($key=='Offer' && isset($items->OfferListing)){
        			return self::getOffersPrices($items->OfferListing);
        		}
        		
        	}
        else:
            return ;
        endif;
    }

    public static function getOffersPrices($data){

    	$offer ='';

    	if(isset($data->AmountSaved)){
    		$offer .= '<strike class=\"offer_strike\">'.$data->AmountSaved->FormattedPrice.'</strike> ';

    	}

    	if(isset($data->PercentageSaved)){
    		$offer .= '<span class=\"offer_percentage\">( '.$data->PercentageSaved.' % off )</span> ';

    	}

    	return $offer;
    }	


     public static function getPagination($page=1, $url){


        if($page==1):
           $pagination=' <ul class="pagination">';
           $pagination.=' <li class="disabled"><span>&laquo;</span></li> ';
           for($i=1; $i<=5; $i++){
                if($i==$page){
                    $pagination.=' <li class="active"><span>'.$i.'</span></li> ';
                }else{
                    $pagination.=' <li><a href="'.$url.'?page='.$i.'">'.$i.'</a></li>';
                }
           }
           $pagination.=' <li><a href="'.$url.'?page='.($page+1).'"><span>&raquo;</span></a></li>';
           $pagination.=' </ul>';
        elseif($page==5):

            $pagination=' <ul class="pagination">';
            //$pagination.=' <li class="disabled"><span>&laquo;</span></li> ';
            $pagination.=' <li><a href="'.$url.'?page='.($page-1).'"><span>&laquo;</span></a></li>';
           for($i=1; $i<=5; $i++){
                if($i==$page){
                    $pagination.=' <li class="active"><span>'.$i.'</span></li> ';
                }else{
                    $pagination.=' <li><a href="'.$url.'?page='.$i.'">'.$i.'</a></li>';
                }
           }
           $pagination.=' <li class="disabled"><span>&raquo;</span></li> ';
           //$pagination.=' <li><a href="'.$url.'?page='.++$page.'"><span>&raquo;</span></a></li>';
           $pagination.=' </ul>';


        else:

           $pagination=' <ul class="pagination">';
            //$pagination.=' <li class="disabled"><span>&laquo;</span></li> ';
            $pagination.=' <li><a href="'.$url.'?page='.($page-1).'"><span>&laquo;</span></a></li>';
           for($i=1; $i<=5; $i++){
                if($i==$page){
                    $pagination.=' <li class="active"><span>'.$i.'</span></li> ';
                }else{
                    $pagination.=' <li><a href="'.$url.'?page='.$i.'">'.$i.'</a></li>';
                }
           }
           //$pagination.=' <li class="disabled"><span>&raquo;</span></li> ';
           $pagination.=' <li><a href="'.$url.'?page='.($page+1).'"><span>&raquo;</span></a></li>';
           $pagination.=' </ul>';


        endif;    

        return $pagination;
    }   


}
