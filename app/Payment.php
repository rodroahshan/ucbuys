<?php

namespace App;

use App\Gateway\WalletMix\WalletSubmission;

class Payment extends Model
{
    public static function get_gateway_type_name_by_key($key){
		if(array_key_exists($key, static::$gateway_type)){
			return static::$gateway_type[$key];
		}
		return '';
	}

	public static $gateway_type = [
		'1' => 'WalletMix',
	];

	

	public function store($order_id, $payment_response, $message){
		// dd($payment_response);
		return $this->create([
				'gateway_type' => 1, // walletmix
				'order_id' => $order_id,
				'trans_token' => $payment_response->token,
				'trans_amount_without_charge' => $payment_response->merchant_amount_bdt,
				'trans_amount' => $payment_response->bank_amount_bdt,
				'trans_charge' => $payment_response->wmx_charge_bdt,
				'trans_discount' => $payment_response->discount_bdt,
				'trans_type' => $payment_response->payment_card,
				'trans_ip' => $payment_response->request_ip,
				'trans_message' => $message,
			]);
	}

	public function submit_for_payment($order_id){

		$WalletSubmission = new WalletSubmission;
		$user = new \App\User;

		$order = \App\Order::find($order_id);

		$product_list = $order->product_list();
		$discount = $order->discount;
		$shipping_charge = $order->delivery_charge;

		$customer_info = $user->customer_info();
        $shipping_info = $user->shipping_info();

		// pr($product_list);
		// pr($discount);
		// pr($shipping_charge);
		// pr($customer_info);
		// pr($shipping_info, 1);




		$WalletSubmission->submit_to_walletMix($order->id, $product_list, $discount, $shipping_charge, $customer_info, $shipping_info);


		//dd($product_list);


		// $products = $cart->products();
  //       $shipping_charge = $cart->shipping;
  //       // dd($shipping_charge);
  //       $customer_info = $user->customer_info();
  //       $shipping_info = $user->shipping_info();

  //   	//$cart->resetCart();
  //       $WalletSubmission->submit_to_walletMix($create_order->id, $products, $discount=0, $shipping_charge, $customer_info, $shipping_info);

	}
}
