<?php

namespace App;


class Pagination extends Model
{


    public static function get_pagination($current_page=50, $total_page=50, $total_show=10, $attr){

    	$left_show = (int) $total_show / 2;
    	$righ_show = (int) $total_show / 2;

    	$start_loop = $current_page - $left_show; //9-5 // 4
    	$start_loop = $start_loop < 1 ? 1 : $start_loop;

    	$end_loop = $current_page + $righ_show; //9+5 // 14
    	$end_loop = $end_loop > $total_page ? $total_page : $end_loop;


        $min_price_amazon = $attr['min_price_amazon_bunch_products'];
        $max_price_amazon = $attr['max_price_amazon_bunch_products'];

        $step = \App\Amazon::get_amazon_step_by_url_page_number($current_page, $category=false);
        $min_price = \App\Amazon::get_amazon_min_price_by_step($step, $min_price_amazon, $max_price_amazon);
        $max_price = \App\Amazon::get_amazon_max_price_by_step($step, $min_price_amazon, $max_price_amazon);


    	$out = '<nav aria-label="Page navigation">
		  			<ul class="pagination">';


    	// if left url found
        $prev_url = self::genarate_prev_pagination_url($current_page, $attr['keyword'], $step, $min_price, $max_price);
		$out .= (1 < $current_page) ? $prev_url : '';


    	for($i = $start_loop; $i <= $end_loop; $i++){
    		
    		$out .= self::genarate_pagination_url($i, $current_page, $attr['keyword'], $step, $min_price, $max_price, $attr['category']);

    	}

		// if left url found
        // $right_page_number = $current_page + 1;
        // $right_url = '<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
    	$right_url = self::genarate_next_pagination_url($current_page, $attr['keyword'], $step, $min_price, $max_price);
		$out .= ($total_page > $current_page) ? $right_url : '';

		$out .= '</ul></nav>';
    	// dd($out);
    	return $out;
    }

    public static function genarate_next_pagination_url($current_page_number, $keyword=null, $step, $min_price=null, $max_price=null){
        $next_page_number = $current_page_number + 1;
        $query_string = self::make_query_string($next_page_number, $keyword, $step, $min_price, $max_price);
        $out = '<li><a href="' . url('products/search') . '?'.$query_string.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
        return $out;
    }

    public static function genarate_prev_pagination_url($current_page_number, $keyword=null, $step, $min_price=null, $max_price=null){
        $prev_page_number = $current_page_number - 1;
        $query_string = self::make_query_string($prev_page_number, $keyword, $step, $min_price, $max_price);
        $out = '<li><a href="' . url('products/search') . '?'.$query_string.'" aria-label="Prev"><span aria-hidden="true">&laquo;</span></a></li>';
        return $out;
    }


    public static function genarate_pagination_url($i, $current_page, $keyword=null, $step, $min_price=null, $max_price=null, $category=null){


        $class = ($current_page == $i) ? 'class="active"' : '';
        

        $query_string = self::make_query_string($i, $keyword, $step, $min_price, $max_price);

        $out = '<li '.$class.'><a href="' . url('products/search') . '?' .$query_string.'">'.$i.'</a></li>';
        return $out;
    }


    public static function make_query_string($i, $keyword, $step, $min_price, $max_price){

        $request = \Request::except('page');
        if(!request('query')){
            $request['query'] = $keyword;
        }
        $request['page'] = $i;

        if($step > 1){
            if($min_price) $request['min_price'] = $min_price;
            if($max_price) $request['max_price'] = $max_price;
            // if($category) $request['category'] = $category;
        }

        $query_string = http_build_query($request);
        return $query_string;
    }
}
