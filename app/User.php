<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public static $search_fields = ['name', 'email'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'name', 'email', 'password', 'telephone', 'city', 'state', 'country', 'postal_code', 'address', 'role', 'country_id'
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function m_role(){
        return $this->belongsTo('\App\Role', 'role', 'key');
    }

    public function m_country(){
        return $this->belongsTo('\App\Country', 'country_id');
    }

    public function orders(){
        return $this->hasMany('\App\Order', 'order_id');
    }

    public function customer_info(){
        $logged_user = auth()->user();
        // dd($logged_user->m_country);

        $customer_info = array(
         "customer_name"     => $logged_user->name,
         "customer_email"    => $logged_user->email,
         //"customer_add"    => $logged_user->address,
         "customer_add"    => 'House',
         "customer_city"    => $logged_user->city,
         "customer_country"    => $logged_user->m_country ? $logged_user->m_country->country_name : 'Not Given',
         "customer_postcode"    => $logged_user->postal_code,
         // "customer_phone"    => $logged_user->telephone,
         "customer_phone"    => '01910000000',
        );
        return $customer_info;
    }


    public function shipping_info(){
        $logged_user = auth()->user();
        $shipping_info = array(
         "shipping_name"     => $logged_user->name,
         "shipping_add"    => $logged_user->address,
         "shipping_city"    => $logged_user->city,
         "shipping_country"    => $logged_user->m_country ? $logged_user->m_country->country_name : 'Not Given',
         "shipping_postCode"    => $logged_user->postal_code,
        );
        return $shipping_info;
    }

    public function update_user_address_phone_city_state(){
        if(!auth()->user()->telephone){
            auth()->user()->update(['telephone' => request('shipping_telephone')]);
        }
        if(!auth()->user()->city){
            auth()->user()->update(['city' => request('shipping_city')]);
        }
        if(!auth()->user()->state){
            auth()->user()->update(['state' => request('shipping_state')]);
        }
        if(!auth()->user()->postal_code){
            auth()->user()->update(['postal_code' => request('shipping_postal_code')]);
        }
        if(!auth()->user()->address){
            auth()->user()->update(['address' => request('shipping_address')]);
        }
        if(!auth()->user()->country_id){
            auth()->user()->update(['country_id' => request('shipping_country_id')]);
        }

    }


    // $shipping_info = array(
    //  "shipping_name" => "Linking Borrows",
    //  "shipping_add" => "1600 Amphitheatre Pkwy, Mountain View",
    //  "shipping_city" => "New York City",
    //  "shipping_country" => "United States",
    //  "shipping_postCode" => "1600",
    // );
}
