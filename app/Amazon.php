<?php

namespace App;


class Amazon extends Model
{
	// Your Access Key ID, as taken from the Your Account page
	protected static $access_key_id = "AKIAJHL5WOYKTZCLZ25A";
	protected static $bdboom_access_key_id = "AKIAJHL5WOYKTZIFWDVC"; // FAKE
	protected static $associateTag = "airposted-21";
	protected static $bdboom_associate_tag = "bdboom-21";
	// Your Secret Key corresponding to the above ID, as taken from the Your Account page
	protected static $secret_key = "wqrErNV+6YYeJMGGIxp5hOCos+Hf0fRkcPuu/ILp";
	// The region you are interested in
	protected static $endpoint = "webservices.amazon.in";
	protected static $uri = "/onca/xml";

	protected static $currency = 'BDT';
	protected static $product_src = 1; // 1 // amazon

	public static $min_max_price_variations = [];

	public static $pagination;

	public static function search($keyword=null, $page=1, $min_price=null, $max_price=null, $category="All"){
		// dd($keyword);
		$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemSearch",
		    "AWSAccessKeyId" => self::$access_key_id,
		    "AssociateTag" => self::$associateTag,
		    "Keywords" => $keyword,
		    "ResponseGroup" => "Images,ItemAttributes,Offers,SearchBins"
		);

		$params['SearchIndex'] = $category ? $category : 'All';
		$params['ItemPage'] = $page ? $page : 1;

		if($min_price) $params['MinimumPrice'] = $min_price . '00';
		if($max_price) $params['MaximumPrice'] = $max_price . '00';

		// dd($params);
		$request_url = self::genarate_url($params);
        $obj = self::curl($request_url, 'obj');
		dd($obj);


        if($obj->Items->TotalResults){
            $products = self::fetch_bunch_obj($obj);
            self::make_pagination($obj, $keyword);
        } else {
            $products = [];
        }
		

		return $products;
    }

    public static function search_by_category($category="All", $keyword=null, $page=1){
    	$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemSearch",
		    "AWSAccessKeyId" => self::$access_key_id,
		    "AssociateTag" => self::$associateTag,
		    "SearchIndex" => $category,
		    "ItemPage" => $page,
		    "Keywords" => $keyword,
		    "ResponseGroup" => "Images,ItemAttributes,Offers"
		);

		$request_url = self::genarate_url($params);
		// dd($request_url);
		$obj = self::curl($request_url, 'obj');
		//dd($obj);
		$products = self::fetch_bunch_obj($obj);
		self::make_pagination($obj);

		return $products;
    }

    public static function item($id, $session=false){
		$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemLookup",
		    "AWSAccessKeyId" => self::$access_key_id,
		    "AssociateTag" => self::$associateTag,
		    "ItemId" => $id,
		    "IdType" => "ASIN",
		    "ResponseGroup" => "Images,ItemAttributes,Offers,VariationMatrix,Reviews"
		);

		$request_url = self::genarate_url($params);
		$obj = self::curl($request_url, 'obj');
		//dd($obj);
		$product = self::fetch_single_obj($obj, $session);

		return $product;
    }

    public static function variation($parent_asin){
		$params = array(
		    "Service" => "AWSECommerceService",
		    "Operation" => "ItemLookup",
		    "AWSAccessKeyId" => self::$access_key_id,
		    "AssociateTag" => self::$associateTag,
		    "ItemId" => $parent_asin,
		    "IdType" => "ASIN",
		    "ResponseGroup" => "VariationMatrix"
		);

		$request_url = self::genarate_url($params);
		$obj = self::curl($request_url, 'obj');
		// dd($obj);
		$variation = self::get_parent_variation($obj);

		return $variation;
    }



    public static function fetch_bunch_obj($obj){
    	if($obj->Items->TotalResults == 1){ // if found single data
    		$single_obj = self::fetch_single_obj($obj);
    		return [$single_obj['id'] => $single_obj];
    	}
    	return self::fetch_from_obj($obj);
    }

    public static function fetch_single_obj($obj, $session=false){
    	return self::fetch_from_obj($obj, $session);
    }


    public static function make_pagination($obj, $keyword=null){
    	//dd($obj);

    	$min_max_variations = self::$min_max_price_variations;

    	//dd($min_max_variations);
    	
    	$min_price_amazon_bunch_products = $min_max_variations['min_price'];
    	$max_price_amazon_bunch_products = $min_max_variations['max_price'];
        // $category = $min_max_variations['search_index'];
    	$category = null;

    	$totalResults = $obj->Items->TotalResults;
    	$totalPages = $obj->Items->TotalPages;
    	$current_page = request('page') ? request('page') : 1;

    	$request = \Request::except('page');
    	$request['page'] = (int) $current_page;
    	//dd($request);
        $out = '';
        if($totalPages){

            $attr = [
                'keyword' => $keyword,
                'min_price_amazon_bunch_products' => $min_price_amazon_bunch_products,
                'max_price_amazon_bunch_products' => $max_price_amazon_bunch_products,
                'category' => $category,
            ];

            $out = \App\Pagination::get_pagination($current_page, $totalPages, $total_show=10, $attr);
        }

  //   	$out = '
  //   	<nav aria-label="Page navigation">
		//   <ul class="pagination">
		//     <li>
		//       <a href="#" aria-label="Previous">
		//         <span aria-hidden="true">&laquo;</span>
		//       </a>
		//     </li>';

		// if($totalPages){
  //   		for ($i = 1; $i < $totalPages; $i++) {
  //   			if($i == 31) break;
  //   			$out .= self::genarate_pagination_url($i, $current_page, $keyword, $min_price_amazon_bunch_products, $max_price_amazon_bunch_products, $category);
  //   		}
  //   	}    
		

		// $out .= '
		//     <li>
		//       <a href="#" aria-label="Next">
		//         <span aria-hidden="true">&raquo;</span>
		//       </a>
		//     </li>
		//   </ul>
		// </nav>';

		self::$pagination = $out;


    	
    }


    

    public static function get_amazon_min_price_by_step($step, $min_price_amazon, $max_price_amazon){
        if($step && $step > 1){
            if(request('min_price')){
               return request('min_price') + (20 * $step);
            } else {
                return $min_price_amazon + (20 * $step);
            }
        }
    }

    public static function get_amazon_max_price_by_step($step, $min_price_amazon, $max_price_amazon){
        if($step && $step > 1){
            if(request('max_price')){
                return request('min_price') + (40 * $step);
            } else {
                return $min_price_amazon + (40 * $step);
            }
        }
    }

    
    // 58 = 3
    public static function get_page_number_and_step($page, $category=false){

    	$calculation_with = 5;

        if($category){
            $calculation_with = 10;
        }
        // dd($calculation_with);
        // dd($page);

    	$step = 1;
    	$item_page = 1;

		// 72
		$devide = $page / $calculation_with; // 14.2
		$step = ceil($devide);
		$devide = floor($devide); // 14
		$root = $devide * $calculation_with; // multiply with 5

		// $item_page = $page - $root;

		if($root == $page){ // 70 = 70
			$item_page = $calculation_with;
		} else {
			$item_page = $page - $root;
		}
    		

        // pr('step: ' . $step);
        // dd('item page ' . (int) $item_page);

        return [
            'item_page' => $item_page,
            'step' => $step
        ];
    	


    }

    public static function get_amazon_item_page_by_url_page_number($page, $category=false){
        return self::get_page_number_and_step($page, $category)['item_page'];
    }

    public static function get_amazon_step_by_url_page_number($page, $category=false){
        return self::get_page_number_and_step($page, $category)['step'];
    }



    public static function curl($url, $output_type='obj'){
    	$handle = curl_init();
    	// Set the url
		curl_setopt($handle, CURLOPT_URL, $url);
		// Set the result output to be a string.
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		 
		$output = curl_exec($handle);
		$err = curl_error($handle);  //if you need
		if($err){
			die($err);
		}
		curl_close($handle);

		$xml = simplexml_load_string($output);

		if($output_type == 'json'){
			return json_encode($xml);
		} elseif($output_type == 'obj'){
			$json = json_encode($xml);
			return json_decode($json);
		}
		

		return $output;
    }

    public static function fetch_from_obj($obj, $session=false){

    	if(!property_exists($obj->Items, 'Item')){
    		return []; // if no product found
    	}

    	$items = $obj->Items->Item;

    	if(is_array($items)){ //if bunch of items array
    		$fetched_array =  self::fetch_bunch_products($items);
    	} else { // if single item
    		$fetched_array = self::fetch_single_product($items, $session);
    	}
    	// dd($fetched_array);
    	return $fetched_array;
    }

    

    public static function get_amazon_offer_price($item){
    	if(property_exists($item, 'OfferSummary') && property_exists($item->OfferSummary, 'LowestNewPrice')){
    		$price = (int) substr( $item->OfferSummary->LowestNewPrice->Amount, 0, -2 );
		} else {
			$price = 0;
		}
		return self::on_local_currency($price);
    }

    public static function get_amazon_price($item){ // without offer price
    	if(property_exists($item->ItemAttributes, 'ListPrice')){
    		$price = (int) substr( $item->ItemAttributes->ListPrice->Amount, 0, -2 );
		} else {
			$price = 0;
		}
		return self::on_local_currency($price);
    }

    public static function get_amazon_net_price($item){ // the offer price// if not offer price found get the main price
    	// dd($item);
    	if(property_exists($item, 'OfferSummary') && property_exists($item->OfferSummary, 'LowestNewPrice')){
    		$price = (int) substr( $item->OfferSummary->LowestNewPrice->Amount, 0, -2 );
		} else {
			if(property_exists($item->ItemAttributes, 'ListPrice')){
				$price = (int) substr( $item->ItemAttributes->ListPrice->Amount, 0, -2 );
    		} else {
    			$price = 0;
    		}
		}
		self::grav_category_and_price($item, $price);
		return self::on_local_currency($price);
    }

    public static function check_exists_on_amazon_index_category($category_name){
    	$search_index_list = self::categories();

    	if(!empty($category_name)){
	    	if(array_key_exists($category_name, $search_index_list)){
	    		return true;
	    	} 
    	}

    	return false;

    }


    public static function grav_category_and_price($item, $net_price){
    	self::$min_max_price_variations['items'][$item->ASIN]['price'] = $net_price;

    	if(!array_key_exists('min_price', self::$min_max_price_variations)){
	    	self::$min_max_price_variations['min_price'] = $net_price;
    	} elseif($net_price < self::$min_max_price_variations['min_price']){
    		self::$min_max_price_variations['min_price'] = $net_price;
    	} else {
            self::$min_max_price_variations['min_price'] = 0;
        }


    	if(!array_key_exists('max_price', self::$min_max_price_variations)){
	    	self::$min_max_price_variations['max_price'] = $net_price;
    	} elseif($net_price > self::$min_max_price_variations['max_price']){
    		self::$min_max_price_variations['max_price'] = $net_price;
    	} else{
            self::$min_max_price_variations['max_price'] = 1000000;
        }

    	$product_index_on_product_group = property_exists($item->ItemAttributes, 'ProductGroup') ? $item->ItemAttributes->ProductGroup : '';
    	$product_index_on_binding = property_exists($item->ItemAttributes, 'Binding') ? $item->ItemAttributes->Binding : '';

    	if(self::check_exists_on_amazon_index_category($product_index_on_product_group)){
    		// found on $item->ItemAttributes->ProductGroup
    		self::$min_max_price_variations['items'][$item->ASIN]['search_index'] = $product_index_on_product_group;

    		if(!array_key_exists('search_index', self::$min_max_price_variations)){
	    		self::$min_max_price_variations['search_index'] = $product_index_on_product_group;
    		}

    	} else {
    		if(self::check_exists_on_amazon_index_category($product_index_on_binding)){
    			// found on $item->ItemAttributes->Binding
    			self::$min_max_price_variations['items'][$item->ASIN]['search_index'] = $product_index_on_binding;

    			if(!array_key_exists('search_index', self::$min_max_price_variations)){
		    		self::$min_max_price_variations['search_index'] = $product_index_on_binding;
	    		}

    		} else {
    			// not found anywhere
	    		self::$min_max_price_variations['items'][$item->ASIN]['search_index'] = 'All';

	    	}
    	} 


    }

    public static function get_shipping_charge($net_price){
    	$shipping_charge = \App\Order::shipping_charge($net_price);
    	return round($shipping_charge);
    }


    public static function get_bdboom_charge($net_price){
    	$bdboom_charge = \App\Order::bdboom_charge($net_price);
    	return round($bdboom_charge);
    }

    public static function get_total_price($item){
    	$net_price = self::get_amazon_net_price($item);
    	if($net_price){
    		$bdboom_charge = \App\Order::bdboom_charge($net_price);
	    	$shipping_charge = \App\Order::shipping_charge($net_price);
	    	$total = $net_price + $bdboom_charge + $shipping_charge;
	    	return round($total, 2);
    	}
    	return 0;
    	
    }

    public static function fetch_bunch_products($items){
    	 // dd($items);
    	$bunch_array = array();
    	foreach ($items as $item) {
			$id = $item->ASIN;
    		$bunch_array[$id]['id'] = $id;

    		$net_price = self::get_amazon_net_price($item); // the offer price // if not offer price found get the main price
	    	$shipping_charge = \App\Order::shipping_charge($net_price);
    		$bdboom_charge = \App\Order::bdboom_charge($net_price);


            $bunch_array[$id]['product_image'] = self::get_product_image($item);

    		// $bunch_array[$id]['large_image'] = property_exists($item, 'LargeImage') ? $item->LargeImage->URL : '';
    		// $bunch_array[$id]['medium_image'] = property_exists($item, 'MediumImage') ? $item->MediumImage->URL : '';
    		$bunch_array[$id]['title'] = $item->ItemAttributes->Title;
    		$bunch_array[$id]['product_src'] = self::$product_src;
    		$bunch_array[$id]['price'] = self::get_amazon_price($item);
    		$bunch_array[$id]['offer_price'] = self::get_amazon_offer_price($item);
    		$bunch_array[$id]['net_price'] = $net_price;
    		$bunch_array[$id]['shipping_charge'] = $shipping_charge;
    		$bunch_array[$id]['bdboom_charge'] = $bdboom_charge;
    		$bunch_array[$id]['total'] = self::get_total_price($item);
		}
		// dd($bunch_array);

		return $bunch_array;
    }

    public static function on_local_currency($amount){

    	if(self::$currency == 'BDT'){
    		$amount = (int) $amount;
			$amount = $amount * 1.13;
    	}
		return round($amount, 2);
    }

    public static function get_amazon_images($item){
    	$images = array();
    	if(property_exists($item, 'ImageSets')){
            if(property_exists($item->ImageSets, 'ImageSet')){

                if(is_array($item->ImageSets->ImageSet)){
                    foreach ($item->ImageSets->ImageSet as $key => $image){
                        $images[$key]['medium_image'] = property_exists($image, 'MediumImage') ? $image->MediumImage->URL : '';
                        $images[$key]['large_image'] = property_exists($image, 'LargeImage') ? $image->LargeImage->URL : '';
                    }
                } else {
                    $images[0]['medium_image'] = property_exists($item->ImageSets->ImageSet, 'MediumImage') ? $item->ImageSets->ImageSet->MediumImage->URL : '';
                    $images[0]['large_image'] = property_exists($item->ImageSets->ImageSet, 'LargeImage') ? $item->ImageSets->ImageSet->LargeImage->URL : '';
                }
                
            }
			
		}
		return $images;
    }

    public static function get_affiliates_url($item){
    	$id = $item->ASIN;
    	// return $item->DetailPageURL;
    	$affiliates_url = "https://www.amazon.in/dp/".$id."/?tag=" . self::$bdboom_associate_tag;
    	return $affiliates_url;
    }

    public static function change_tag_to_my_affiliates($url) {
	    $url = str_replace(self::$associateTag, self::$bdboom_associate_tag, $url);
	    $url = str_replace(self::$access_key_id, self::$bdboom_access_key_id, $url);
	    return $url;
	}

	public static function get_reviews_iframe_url($item){
		if(property_exists($item, 'CustomerReviews')){
			return self::change_tag_to_my_affiliates($item->CustomerReviews->IFrameURL);
		} else {
			return '#';
		}
    }

    public static function get_detail_page_url($item){
		if(property_exists($item, 'DetailPageURL')){
			return self::change_tag_to_my_affiliates($item->DetailPageURL);
		} else {
			return '#';
		}
    }

    public static function get_feature($item){
    	$feature = [];
    	if(property_exists($item->ItemAttributes, 'Feature')){
    		if(is_array($item->ItemAttributes->Feature)){
    			$feature = $item->ItemAttributes->Feature;
    		} else {
    			$feature[] = $item->ItemAttributes->Feature;
    		}
    	} 
    	return $feature;
    }



    public static function get_item_variation($item){
    	// dd($item);
    	$variation = [];
    	if(property_exists($item, 'Variations')){
    		if(property_exists($item->Variations, 'Item')){
    			if(is_array($item->Variations->Item) && count($item->Variations->Item)){
    				foreach ($item->Variations->Item as $variant) {
    					$id = property_exists($variant, 'ASIN') ? $variant->ASIN : '';
    					if(property_exists($variant, 'VariationAttributes') && property_exists($variant->VariationAttributes, 'VariationAttribute')){
    						if(is_array($variant->VariationAttributes->VariationAttribute)){
    							foreach ($variant->VariationAttributes->VariationAttribute as $variant_attr) {
	    							$variation[$id][$variant_attr->Name] = $variant_attr->Value;
	    						}
    						} else {
    							$variation[$id][$variant->VariationAttributes->VariationAttribute->Name] = $variant->VariationAttributes->VariationAttribute->Value;
    						}
    						
    					}
    				}
    			}
    		}
    	}
    	return $variation;
    }


    public static function get_parent_variation($obj){
    	$variation = [];
    	if(!property_exists($obj->Items, 'Item')){
    		return []; // if no product found
    	}
    	$item = $obj->Items->Item;
    	$variation = self::get_item_variation($item);
    	
    	return $variation;
    }

    public static function get_dimensions($item){
        if(property_exists($item, 'ItemAttributes')){
            if(property_exists($item->ItemAttributes, 'ItemDimensions')){
                return [
                    'height' => property_exists($item->ItemAttributes->ItemDimensions, 'Height') ? $item->ItemAttributes->ItemDimensions->Height : '',
                    'length' => property_exists($item->ItemAttributes->ItemDimensions, 'Length') ? $item->ItemAttributes->ItemDimensions->Length : '',
                    'weight' => property_exists($item->ItemAttributes->ItemDimensions, 'Weight') ? $item->ItemAttributes->ItemDimensions->Weight : '',
                    'width' => property_exists($item->ItemAttributes->ItemDimensions, 'Width') ? $item->ItemAttributes->ItemDimensions->Width : '',
                ];
            }
        }
    }

    public static function get_product_image($item){
        if(property_exists($item, 'LargeImage')){
            return $item->LargeImage->URL;
        }

        if(property_exists($item, 'MediumImage')){
            return $item->MediumImage->URL;
        }

        if(property_exists($item, 'SmallImage')){
            return $item->SmallImage->URL;
        }

        return self::find_any_image_from_image_set($item);


    }


    public static function find_any_image_from_image_set($item){

        if(property_exists($item, 'ImageSets')){
            if(property_exists($item->ImageSets, 'ImageSet')){

                if(is_array($item->ImageSets->ImageSet)){
                    foreach ($item->ImageSets->ImageSet as $key => $image){

                        if(property_exists($image, 'MediumImage')){
                            return $image->MediumImage->URL;
                        }

                        if(property_exists($image, 'LargeImage')){
                            return $image->LargeImage->URL;
                        }

                    }
                } else {

                    if(property_exists($item->ImageSets->ImageSet, 'MediumImage')){
                            return $item->ImageSets->ImageSet->MediumImage->URL;
                        }

                    if(property_exists($item->ImageSets->ImageSet, 'LargeImage')){
                             return $item->ImageSets->ImageSet->LargeImage->URL;
                        }   

                }
                
            }
            
        }
        return '';

    }


    public static function fetch_single_product($item, $session=false){
    	// dd($item);
    	$single_item_array = [];

    	$id = $item->ASIN;
		$single_item_array['id'] = $id;
		$single_item_array['parent_id'] = property_exists($item, 'ParentASIN') ? $item->ParentASIN : null;

		$net_price = self::get_amazon_net_price($item); // the offer price // if not offer price found get the main price
    	$shipping_charge = \App\Order::shipping_charge($net_price);
		$bdboom_charge = \App\Order::bdboom_charge($net_price);

        $single_item_array['product_image'] = self::get_product_image($item);

		// $single_item_array['large_image'] = property_exists($item, 'LargeImage') ? $item->LargeImage->URL : '';
		// $single_item_array['medium_image'] = property_exists($item, 'MediumImage') ? $item->MediumImage->URL : '';
		$single_item_array['title'] = $item->ItemAttributes->Title;

		$single_item_array['product_src'] = self::$product_src;

		$single_item_array['price'] = self::get_amazon_price($item);
		$single_item_array['offer_price'] = self::get_amazon_offer_price($item);
		$single_item_array['net_price'] = $net_price;
		$single_item_array['shipping_charge'] = $shipping_charge;
		$single_item_array['bdboom_charge'] = $bdboom_charge;
		$single_item_array['total'] = self::get_total_price($item);
        $single_item_array['affiliates_url'] = self::get_affiliates_url($item);

        if(!$session){
    		$single_item_array['dimensions'] = self::get_dimensions($item);
			$single_item_array['images'] = self::get_amazon_images($item);
			//$single_item_array['variations'] = self::get_item_variation($item);
			$single_item_array['customer_reviews'] = self::get_reviews_iframe_url($item);
			$single_item_array['feature'] = self::get_feature($item);
		}

		$single_item_array['detail_page_url'] = self::get_detail_page_url($item);



		// dd($single_item_array);

		return $single_item_array;
    }




    public static function genarate_url($params){
    	// Set current timestamp if not set
		if (!isset($params["Timestamp"])) {
		    $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		}

		// Sort the parameters by key
		ksort($params);

		$pairs = array();

		foreach ($params as $key => $value) {
		    array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}

		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);

		// Generate the string to be signed
		$string_to_sign = "GET\n".self::$endpoint."\n".self::$uri."\n".$canonical_query_string;

		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, self::$secret_key, true));

		// Generate the signed URL
		$request_url = 'http://'.self::$endpoint.self::$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		return $request_url;
    }

    public static function categories(){
    	return array(
    		'All' => [
		    			'department' => 'All Departments',
		    			'root_browse_nodes' => '',
    		    	 ],
			'Apparel' => [
		    			'department' => 'Clothing & Accessories',
		    			'root_browse_nodes' => '1571272031',
    		    	 ], 
    		'Appliances' => [
		    			'department' => 'Appliances',
		    			'root_browse_nodes' => '5122349031',
    		    	 ],    	  
    		'Automotive' => [
		    			'department' => 'Car & Motorbike',
		    			'root_browse_nodes' => '4772061031',
    		    	 ],    	     	    		    	 
    		'Baby' => [
		    			'department' => 'Baby',
		    			'root_browse_nodes' => '1571275031',
    		    	 ],
    		'Beauty' => [
		    			'department' => 'Beauty',
		    			'root_browse_nodes' => '1355017031',
    		    	 ],
    		'Books' => [
		    			'department' => 'Books',
		    			'root_browse_nodes' => '976390031',
    		    	 ],
    		'DVD' => [
		    			'department' => 'Movies & TV Shows',
		    			'root_browse_nodes' => '976417031',
    		    	 ],
    		'Electronics' => [
		    			'department' => 'Electronics',
		    			'root_browse_nodes' => '976420031',
    		    	 ],
    		'Furniture' => [
		    			'department' => 'Furniture',
		    			'root_browse_nodes' => '1380441031',
    		    	 ],
    		'GiftCards' => [
		    			'department' => 'Gift Cards',
		    			'root_browse_nodes' => '3704983031',
    		    	 ],    	     	     	     	     	     	 
            'Grocery' => [
		    			'department' => 'Gourmet & Specialty Foods',
		    			'root_browse_nodes' => '2454179031',
    		    	 ],
    		'HealthPersonalCare' => [
		    			'department' => 'Health & Personal Care',
		    			'root_browse_nodes' => '1350385031',
    		    	 ],
    		'HomeGarden' => [
		    			'department' => 'Home & Kitchen',
		    			'root_browse_nodes' => '2454176031',
    		    	 ],
    		'Industrial' => [
		    			'department' => 'Industrial & Scientific',
		    			'root_browse_nodes' => '5866079031',
    		    	 ],
    		'Jewelry' => [
		    			'department' => 'Jewellery',
		    			'root_browse_nodes' => '1951049031',
    		    	 ],
    		'KindleStore' => [
		    			'department' => 'Kindle Store',
		    			'root_browse_nodes' => '1571278031',
    		    	 ],
    		'LawnAndGarden' => [
		    			'department' => 'Lawn & Garden',
		    			'root_browse_nodes' => '2454175031',
    		    	 ],
    		'Luggage' => [
		    			'department' => 'Luggage & Bags',
		    			'root_browse_nodes' => '2454170031',
    		    	 ],
    		'LuxuryBeauty' => [
		    			'department' => 'Luxury Beauty',
		    			'root_browse_nodes' => '5311359031',
    		    	 ],
    		'Music' => [
		    			'department' => 'Music',
		    			'root_browse_nodes' => '976446031',
    		    	 ],
    		'MusicalInstruments' => [
		    			'department' => 'Musical Instruments',
		    			'root_browse_nodes' => '3677698031',
    		    	 ],    	     	     	     	     	     	     	     	     	 

    	);
    }



    
}
