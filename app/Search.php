<?php

namespace App;

class Search extends Model
{

    public static function lookup_type($query){
      $query = request('query');
          // dd($query);
          if($query){
             $product_src_domain_name = self::get_domain_name($query); // amazon.in or flipkart.com
             // dd($product_src_domain_name);
             if(!$product_src_domain_name){
                // product source not found
                // go with normal search
                $lookup_type = 'keyword';
                // self::amazon_keyword_search($query);
             } else {
                $lookup_type = $product_src_domain_name;
             }
          }
          return $lookup_type;
      }

    public static function search_by_url($query, $product_src){

      if($product_src == 'amazon.in'){
          self::search_in_amazon($query);
      }

    }

    public static function get_amazon_asin_id_by_query($query){
        // find asin id on amazon url
        $asin_before_segment_list = ['dp', 'product'];

        // dd($query);
        $uri_path = parse_url($query, PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        // dd($uri_segments);

        foreach ($asin_before_segment_list as $asin_before_segment) {
          // dd($asin_before_segment);
          $dp_or_product_index = array_search($asin_before_segment, $uri_segments);

          if($dp_or_product_index){
              $ASIN_ID_KEY = $dp_or_product_index + 1;
              $ASIN_ID = $uri_segments[$ASIN_ID_KEY];
              return $ASIN_ID;
          }
        }

        return false;
    }


    public static function amazon_keyword_search($query){
        $page = request('page');


        $min_price = request('min_price');
        $max_price = request('max_price');

        $category = request('category');


        $get_page_number_and_step = \App\Amazon::get_page_number_and_step($page, $category);

        $amazon_item_page = $get_page_number_and_step['item_page'];
        $step = $get_page_number_and_step['step'];

        // $min_price = \App\Amazon::get_min_price($step);
        // $max_price = \App\Amazon::get_max_price($step);

        $products = \App\Amazon::search($query, $amazon_item_page, $min_price, $max_price, $category);
        return $products;
    }

    public static function get_domain_name($url = SITE_URL) {
	    preg_match("/[a-z0-9\-]{1,63}\.[a-z\.]{2,6}$/", parse_url($url, PHP_URL_HOST), $_domain_tld);
	    if($_domain_tld){
	    	return $_domain_tld[0];
	    }
	    return false;
	}


}
