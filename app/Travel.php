<?php

namespace App;

class Travel extends Model
{
    public function cntry_fr(){
    	return $this->belongsTo('\App\Country', 'country_from');
    }

    public function cntry_to(){
    	return $this->belongsTo('\App\Country', 'country_to');
    }

    public function user(){
    	return $this->belongsTo('\App\User');
    }
}
