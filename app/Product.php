<?php

namespace App;

class Product extends Model
{
    public static function get_variations($parent_asin, $current_asin){
        $variations = \App\Amazon::variation($parent_asin);
        // dd($variations);
    	$output = '';
    	foreach ($variations as $ASIN => $variant) {
            $selected = $current_asin == $ASIN ? 'selected' : '';
            if(array_key_exists('Size', $variant) && array_key_exists('Color', $variant)){

                $size = $variant['Size'];
                $color = $variant['Color'];
                $output .= "<option value='$ASIN' {$selected}>$size $color</option>";
                
            } else {
                if(array_key_exists('Size', $variant)){
                    $size = $variant['Size'];
                    $output .= "<option value='$ASIN' {$selected}>$size</option>";
                }

                if(array_key_exists('Color', $variant)){
                    $color = $variant['Color'];
                    $output .= "<option value='$ASIN' {$selected}>$color</option>";
                }
            }
    	}
    	// dd($output);
    	return $output;
    }
}
