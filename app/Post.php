<?php

namespace App;

class Post extends Model
{
    protected $guarded = [];

    public function comments(){
    	return $this->hasMany('\App\Comment');
    }

    public function user(){
        return $this->belongsTo('\App\User');
    }

    public function scopeFilter($query, $param){
        if($month = $param['month']){
            $query->whereMonth('created_at', \Carbon\Carbon::parse($month)->month);
        }

        if($year = $param['year']){
            $query->whereYear('created_at', $year);
        }
        return $query;
    }

    public function add_comments($body){
    	// \App\Comment::create([
    	// 	'body' => $body,
    	// 	'post_id' => $this->id
    	// 	]);
    	$this->comments()->create(['body' => $body]);
    }

    public static function archives(){
        return self::selectRaw('year(created_at) as year, monthname(created_at) as month, count(*) as published')->groupBy('year', 'month')->orderByRaw('month(created_at) asc')->get();
    }
}
