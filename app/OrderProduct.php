<?php

namespace App;

class OrderProduct extends Model
{
	
    public function order(){
    	return $this->belongsTo(Order::class);
    }

    public static $product_src = [
		'0' => ['name' => 'Other', 'url' => ''],
		'1' => ['name' => 'Amazon', 'url' => 'amazon.in'],
		'2' => ['name' => 'Flipcart', 'url' => 'flipkart.com'],
	];

	public static function get_product_src_by_key($key, $lookup='name'){
		//lookup = name/url
		if(array_key_exists($key, static::$product_src)){
			return static::$product_src[$key][$lookup];
		}
		return '';
	}
    
    public static function save_order_products($order_id){
    	$cart = \App\Cart::cart_items();
    	// dd($cart);
    	foreach ($cart as $id => $item) {

    			$master_total = round($item['total'] * $item['qty'], 2);

	    		\App\OrderProduct::create([
	    			'order_id' => $order_id,
	    			'product_id' => $item['id'],
					'product_src' => $item['product_src'],
					'product_image' => $item['product_image'],
					'title' => $item['title'],
					'qty' => $item['qty'],
					'price' => $item['price'],
					'offer_price' => $item['offer_price'],
					'net_price' => $item['net_price'],
					'shipping_charge' => $item['shipping_charge'],
					'bdboom_charge' => $item['bdboom_charge'],
					'total' => $item['total'],
					'master_total' => $master_total,
					'affiliates_url' => $item['affiliates_url'],
					'detail_page_url' => $item['detail_page_url'],

		            'note' => '',
	    		]);
	    	}
	    return true;	
    }
}
