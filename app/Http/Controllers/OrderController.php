<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gateway\WalletMix\WalletSubmission;

class OrderController extends Controller
{

    

    public function create(\App\Order $order, \App\Payment $payment, \App\User $user, \App\Cart $cart){

    	$create_order = $order->create_order(); // creating order and updating shipping address
        $user->update_user_address_phone_city_state(); // update address_phone_city_state if not updated on user phone, city, state, address
    	$save_order_products = \App\OrderProduct::save_order_products($create_order->id);

        $cart->resetCart();
        $payment->submit_for_payment($create_order->id);

        // $WalletSubmission->submit();

    	// if($save_order_products){
	    // 	return redirect('/dashboard/my_orders')->withErrors('Order Placed Successfully');
    	// }
    	
    }

    public function pay_now($order_id){
        $payment = new \App\Payment;
        $payment->submit_for_payment($order_id);
    }

    public function confirm_payment(\App\Payment $payment){
        //dd($_POST);
        $WalletSubmission = new WalletSubmission();
        $payment_callback = $WalletSubmission->call_back_walletmix();
        $message = $payment_callback['message'];

        if($payment_callback['trans']){
            $order_id = $payment_callback['response']->merchant_order_id;
            $payment_store = $payment->store($order_id, $payment_callback['response'], $message);

            if($payment_callback['success']){
                \App\Order::find($order_id)->update(['payment_id' => $payment_store->id, 'payment_status' => 1, 'delivery_status' => 1]);
                return redirect()->action('OrderController@my_order_details', $order_id)->withErrors('Order Placed Succesfully');
            }
        } else {
            return redirect()->action('OrderController@my_orders')->withErrors($message);
        }
    }


    public function my_orders(){
    	$user_id = auth()->user()->id;
    	$orders = \App\Order::where('user_id', $user_id)->latest()->paginate(5);
    	//dd($orders);
    	return view('dashboard.pages.orders.my_orders', compact('orders'));
    }

    public function my_order_details($order_id){
        $order = \App\Order::find($order_id);
    	return view('dashboard.pages.orders.my_order_details', compact('order'));
    }
}
