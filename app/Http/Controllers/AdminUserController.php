<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    

    private $validate_rules = [
            'name' => 'required|max:255',
            'role' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
    ];

    private $validate_rules_update = [
            'name' => 'required|max:255',
            'role' => 'required',
            'email' => 'required|email',
    ];

    public function index(){
    	$users = \App\User::paginate(10);
    	return view('admin.pages.users.index', compact('users'));
    }

    public function search(Request $request)
    {
    
        $keyword = $request->keyword;
        $search_fields = \App\User::$search_fields;
        $result =   new \App\User;

        if(request('keyword')){

            if(count($search_fields)){
                foreach ($search_fields as $field) {
                    $result = $result->orWhere( $field, 'like', '%' . $keyword . '%');
                }
            }
        }

        return view('admin.pages.users.index', ['users'=> $result->paginate(10)]);
    }

    public function show($id){
    	$user = \App\User::find($id);
    	return view('admin.pages.users.show', compact('user'));
    }

    public function edit($id){
        $user = \App\User::find($id);
        return view('admin.pages.users.edit', compact('user'));
    }

    public function create(){
    	return view('admin.pages.users.create');
    }

    public function update($id, Request $request){
        $user = \App\User::find($id);
        $this->validate($request, $this->validate_rules_update);
        $post_data = request()->all();
        $update = $user->update($post_data);

        if($update){
            return redirect('/admin/users')->with('message', 'User Updated Succesfully');
        }
    }

    public function store(Request $request){
        $this->validate($request, $this->validate_rules);

        $post_data = request()->all();
        $post_data['password'] = md5($post_data['password']);

        $save = \App\User::create($post_data);

        if($save){
            return redirect('/admin/users')->with('message', 'User Added Succesfully');
        }
    }

    public function destroy($id){
        $delete = \App\User::find($id)->delete();
        if($delete){
            return back()->with('message', 'User Deleted Succesfully');
        }
    }
}
