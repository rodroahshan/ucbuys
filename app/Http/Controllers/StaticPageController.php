<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function about_us(){
    	return view('pages.static.about-us');
    }
	public function faq(){
		return view('pages.static.faq');
	}
	public function how_it_works(){
		return view('pages.static.how-it-works');
	}
	public function why_us(){
		return view('pages.static.why-us');
	}
	public function contact_us(){
		return view('pages.static.contact-us');
	}
}
