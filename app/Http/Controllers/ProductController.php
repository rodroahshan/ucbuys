<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function pagination(){
        return view('pages.products.pagination');
    }

    public function index(){
        // $query = request('query') ? request('query') : 'Saree';
        $query = request('query') ? request('query') : \App\AmazonCategory::random_category_name();
        $category = request('category') ? request('category') : 'All';
        $page = request('page');
        $products = \App\Search::amazon_keyword_search($query);
        $pagination = \App\Amazon::$pagination;


        return view('pages.products.index', compact('products', 'pagination'));
    }

    public function search(){

        $query = request('query');
        $lookup_type = \App\Search::lookup_type($query);
        // dd($lookup_type);

        if($lookup_type == 'keyword'){
            $products = \App\Search::amazon_keyword_search($query);
            // dd(\App\Amazon::$min_max_price_variations);
        }

        if($lookup_type == 'amazon.in'){
            $ASIN_ID = \App\Search::get_amazon_asin_id_by_query($query);
            if($ASIN_ID){
                return $this->show($ASIN_ID);
            }
        }
        // dd($query);
        // $products = \App\Search::lookup($query);

        // if($query){
        //    $uri_path = parse_url($query, PHP_URL_PATH);
        //     $uri_segments = explode('/', $uri_path);
        //     // dd($uri_segments);
        //     $dp_index_key = array_search("dp",$uri_segments);
        //     $product_index_key = array_search("product",$uri_segments);
        //     if($dp_index_key){
        //         $asin_id_key = $dp_index_key + 1;
        //         $asin_id = $uri_segments[$asin_id_key];
        //         return $this->show($asin_id);
        //     }

        //     if($product_index_key){
        //         $asin_id_key = $product_index_key + 1;
        //         $asin_id = $uri_segments[$asin_id_key];
        //         return $this->show($asin_id);
        //     }
        // }
        



        // $query = request('query') ? request('query') : 'Saree';
        // $category = request('category') ? request('category') : 'All';
        // $page = request('page');

        // if(request('category')){
        //     $products = \App\Amazon::search_by_category($category, $category, $page);
        // } else {
        //     $products = \App\Amazon::search($query, $page);
        // }

        $pagination = \App\Amazon::$pagination;
        return view('pages.products.index', compact('products', 'pagination'));
    }

    // public function search(){
    //     //dd(session()->all());
    //     $query = request('query') ? request('query') : 'Iphone';
    //     $category = request('category') ? request('category') : 'All';
    //     $page = request('page');

    //     if(request('category')){
    //         dd("Cateigrt"); 
    //         $products = \App\Amazon::search_by_category($category, $category, $page=1);
    //     } else {
    //         dd("Only Search"); 
    //         $products = \App\Amazon::search($query, $page);
    //     }

    //     $pagination = \App\Amazon::$pagination;
    //     return view('pages.products.index', compact('products', 'pagination'));
    // }
    public function checkout(){
        $total_cart = \App\Cart::get_total_cart($on_bdt = false);
        return view('pages.products.checkout', compact('total_cart'));
    }

    public function payment(){
        return view('pages.products.payment', compact('cart'));
    }

    public function show($id){
       // dd(request()->all());
        // dd($id);
        // \App\Amazon::variation($id);
    	$product = \App\Amazon::item($id);
    	// dd(count($product));
		return view('pages.products.show', compact('product'));
    }

    public function addToCart(\App\Cart $cart){
        $item_id = request('item_id');
        //dd($item_id);
        $add_to_cart = $cart->productAddToCart($item_id);

        if($add_to_cart){
            if(request('ajax')){ // if ajax
                $data = [
                    'cart' => \App\Cart::get_total_cart($on_bdt = true),
                ];
                echo json_encode($data);
                return;
            } else {
                return back()->withErrors('Product Added Successfully');
            }
        }
    }

    public function plus_item(\App\Cart $cart){
        $item_id = request('item_id');

        if($cart->plus_item($item_id)){
            if(request('ajax')){ // if ajax
                $data = [
                    'cart' => \App\Cart::get_total_cart($on_bdt = true),
                    'product_qty' => \App\Cart::get_net_qty_by_id_from_session($item_id),
                ];
                echo json_encode($data);
                return;
            } else {
                 return back();
            }
        }
    }

    public function minus_item(\App\Cart $cart){
        $item_id = request('item_id');
        if($cart->minus_item($item_id)){
            if(request('ajax')){ // if ajax
                $data = [
                    'cart' => \App\Cart::get_total_cart($on_bdt = true),
                    'product_qty' => \App\Cart::get_net_qty_by_id_from_session($item_id),
                ];
                echo json_encode($data);
                return;
            } else {
                 return back();
            }
        }
    }


    public function remove_item(\App\Cart $cart){
        $item_id = request('item_id');
        if($cart->remove_item($item_id)){
            if(request('ajax')){ // if ajax
                $data = [
                    'cart' => \App\Cart::get_total_cart($on_bdt = true),
                ];
                echo json_encode($data);
                return;
            } else {
                 return back();
            }
        }
    }


    public function cart(){
        $cart_items = \App\Cart::cart_items();
        $total_cart = \App\Cart::get_total_cart($on_bdt = false);
        return view('pages.products.cart', compact('cart_items', 'total_cart'));
    }

    public function resetCart(\App\Cart $cart){
        if($cart->resetCart()){
            return back();
        }
    }

    public function resetAll(\App\Cart $cart){
        $cart->resetAll();
         return back();
        
    }
}
