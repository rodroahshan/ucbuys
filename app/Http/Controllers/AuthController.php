<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('guest', ['except' => 'logout']);
    }
    public function register(){
    	return view('auth.register');
    }

    public function traveler_register(){
        return view('traveler.register');
    }

    public function login(){
    	return view('auth.login');
    }

    public function store(Request $request){

    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email|unique:users',
    		'password' => 'required|confirmed'
    		]);
        //dd(md5($request->password));
    	//dd(request()->all());
    	$user = 
	    	\App\User::create([
	    		'name' => $request->name,
	    		'email' => $request->email,
	    		'telephone' => $request->telephone,
                'role' => $request->role,
	    		'password' => md5($request->password)
	    	]);

	    auth()->login($user);	

    	return back()->withInput()->withErrors('Success');

    }

    public function login_check(Request $request){
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required'
    		]);
        //dd(md5($request->password));
    	$user = \App\User::where(['email' => $request->email, 'password' => md5($request->password)])->first();
        //dd($user);
    	if($user){
    		auth()->login($user);
    		return back()->withErrors('Logged in succesfully');
    	} else {
            return back()->withErrors('Not Matched');
        }
    }

    public function logout(){
    	\Auth::logout();
        return redirect('/auth/login')->withErrors('Successfully Logged Out');
    	

    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

        // OAuth Two Providers
        $token = $user->token;
        $refreshToken = $user->refreshToken; // not always provided
        $expiresIn = $user->expiresIn;

        // OAuth One Providers
        $token = $user->token;
        //$tokenSecret = $user->tokenSecret;

        // All Providers
        $user->getId();
        $user->getNickname();
        $user->getName();
        $user->getEmail();
        $user->getAvatar();
        // $user->token;

        //dd($user);

        $found_user = \App\User::where('email', $user->getEmail())->first();

        if(!$found_user){
            $found_user = 
                \App\User::create([
                    'name' => $user->getName(),
                    'email' => $user->getEmail(),
                    'role' => 2,
                    'password' => md5($user->getId())
                ]);
        }

        auth()->login($found_user);
        return redirect()->home()->withErrors('Logged in succesfully');
    }
}
