<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function update_user(){
    	// dd(request()->all());
    	$update = auth()->user()->update(request()->all());
    	// dd($update);
    	if($update){
	    	return back()->withErrors('Updated Successfully');
    	}

    }

    public function update_password(){
    	$this->validate(request(), [
    		'old_password' => 'required',
    		'password' => 'required|confirmed',
    	]);

    	$old_password = md5(request('old_password'));
    	$new_password = md5(request('password'));

    	if(!auth()->user()->where('password', $old_password)->first()){
    		return back()->withErrors('Old password not matched');
    	}

    	$update_password = auth()->user()->update(['password' => $new_password]);

    	if($update_password){
    		return back()->withErrors('Password Updated Successfully');
    	}

    }
}
