<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AwsProduct;
use STDclass;
use Session;
use Cookie;

class AwsProductsController extends Controller
{

    var $xml;
    var $error_message;
    var $error=0;

    /**
     * Wishlist response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wishlist()
    {   
        //Session::flush();
       if (Session::has('add-to-wishlist')):
           $data = Session::get('add-to-wishlist');  

           //echo'<pre>';
           //print_r($data);
          //echo'</pre>';
           return view('public.products.wishlist', compact('data') );
        else:
             return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;    
    }

    /**
     * add to Cart response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cart()
    {   
       //Session::flush();
       if (Session::has('add-to-cart')):
           $data = Session::get('add-to-cart');  

           //echo'<pre>';
           //print_r($data);
           //echo'</pre>';

           return view('public.products.cart', compact('data') );
        else:
             return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;    
    }


    /**
     * add to Cart plus Item response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCartPlus(Request $request)
    {   
        
        //Session::flush();
       if (Session::has('add-to-cart') && $request->get('id')):
           $data = Session::get('add-to-cart'); 
           $qty = $data['qty'][$request->get('id')]+1; 
           if($qty<=5):
                $request->session()->put("add-to-cart.qty.".$request->get('id'), $qty);
                $total_count = $this->getAddToCartTotal($request->session()->get('add-to-cart'));
                $request->session()->put("add-to-cart.total_qty", $total_count);
                //print_r( Session::get('add-to-cart'));
                $total = array('qty'=>$qty,'qty_total'=>$total_count);
                return response()->json($total);    
           endif; 
        else:
             return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;    
    }


    /**
     * add to Cart Minus Item response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCartMinus(Request $request)
    {   
        
        //Session::flush();
       if (Session::has('add-to-cart') && $request->get('id')):
           $data = Session::get('add-to-cart'); 
           $qty = $data['qty'][$request->get('id')]-1; 
           if($qty>=1):
                $request->session()->put("add-to-cart.qty.".$request->get('id'), $qty);
                $total_count = $this->getAddToCartTotal($request->session()->get('add-to-cart'));
                $request->session()->put("add-to-cart.total_qty", $total_count);
                //print_r( Session::get('add-to-cart'));
                $total=array('qty'=>$qty,'qty_total'=>$total_count);
                return response()->json($total); 
           endif; 
        else:
             return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;    
    }

    /**
     * add to Cart Delete Item response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCartDelete(Request $request)
    {   

        $id='';
        //Session::flush();
       if (Session::has('add-to-cart') && $request->get('id')):
           $data = Session::get('add-to-cart'); 
            foreach ($data['id'] as $key => $item) {
                if($item==$request->get('id')){
                    $id=$key;
                }
            }

            if($id>=0){
               $request->session()->pull('add-to-cart.id.'.$id); 
               $request->session()->pull('add-to-cart.qty.'.$request->get('id'));
               $request->session()->pull('add-to-cart.title.'.$request->get('id'));
               $request->session()->pull('add-to-cart.link.'.$request->get('id'));
               $request->session()->pull('add-to-cart.price.'.$request->get('id'));
               $request->session()->pull('add-to-cart.image.'.$request->get('id'));

               $total_count = $this->getAddToCartTotal($request->session()->get('add-to-cart'));
               $request->session()->put("add-to-cart.total_qty", $total_count);
               //print_r( Session::get('add-to-cart'));
               $total = array('status'=>1,'qty_total'=>$total_count);
               return response()->json($total);
            }  
            $total = array('status'=>0,'qty_total'=>$total_count);
            return response()->json($total); 
        else:
             return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;    
    }



    /**
     * add to Cart Ajax response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCartwishlist(Request $request)
    {   
        $count = 1;
        $id_count=1;
        $ofset = 0;

        //$request->session()->flush();

        if ($request->session()->has('add-to-wishlist')) {

            $data = $request->session()->get('add-to-wishlist.id');
            $count = count($data)+1;
            foreach ($data as $key => $id) {
                if($request->get('id')==$id){
                    $ofset = $request->session()->get("add-to-wishlist.qty.$id");
                    $id_count = $ofset+1;
                    $count = $key;
                }
            }
        }

        if($id_count==1){
            $request->session()->put("add-to-wishlist.id.$count", $request->get('id'));
            $request->session()->put("add-to-wishlist.qty.".$request->get('id'), $id_count);
            $info = $this->get_item_data($this->getAWSItemSearchData(AwsProduct::getAWSItemLookup($request->get('id'))));
            $request->session()->put("add-to-wishlist.title.".$request->get('id'), $info['title'][0]);
            $request->session()->put("add-to-wishlist.link.".$request->get('id'), $info['link'][0]);
            $request->session()->put("add-to-wishlist.image.".$request->get('id'), $info['mediumImage'][0]);  
            $price = (array)$info['price'][0];
            if(count($price)){
                $request->session()->put("add-to-wishlist.price.".$request->get('id'), $price['FormattedPrice']);
            }else{
               $request->session()->put("add-to-wishlist.price.".$request->get('id'), ''); 
            }   
        }
        //echo $id_count;
        //print_r( Session::get('add-to-wishlist'));   
        return $id_count==1?'Successfully Added to wishlist':'Already Added to wishlist';  
    }


    /**
     * Wishlist Items Delete Item response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCartwishlistDelete(Request $request)
    {   

        $id='';
        //Session::flush();
       if (Session::has('add-to-wishlist') && $request->get('id')):
           $data = Session::get('add-to-wishlist'); 
            foreach ($data['id'] as $key => $item) {
                if($item==$request->get('id')){
                    $id=$key;
                }
            }

            if($id>=0){
               $request->session()->pull('add-to-wishlist.id.'.$id); 
               $request->session()->pull('add-to-wishlist.qty.'.$request->get('id'));
               $request->session()->pull('add-to-wishlist.title.'.$request->get('id'));
               $request->session()->pull('add-to-wishlist.link.'.$request->get('id'));
               $request->session()->pull('add-to-wishlist.price.'.$request->get('id'));
               $request->session()->pull('add-to-wishlist.image.'.$request->get('id'));

               $total_count = $this->getAddToCartTotal($request->session()->get('add-to-wishlist'));
               $request->session()->put("add-to-wishlist.total_qty", $total_count);
               //print_r( Session::get('add-to-cart'));
               $total = array('status'=>1,'qty_total'=>$total_count);
               return response()->json($total);
            }  
            $total = array('status'=>0,'qty_total'=>$total_count);
            return response()->json($total); 
        else:
             return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;    
    }

    

    /**
     * add to Cart Ajax response of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCart(Request $request)
    {   
        $count = 1;
        $id_count=1;
        $ofset = 0;

       //$request->session()->flush();

        if ($request->session()->has('add-to-cart')) {

            $data = $request->session()->get('add-to-cart.id');
            $count = count($data)+1;
            foreach ($data as $key => $id) {
                if($request->get('id')==$id){
                    $ofset = $request->session()->get("add-to-cart.qty.$id");
                    $id_count = $ofset+1;
                    $count = $key;
                }
            }
        }

        if($id_count>=1 && $id_count<=5):
            $request->session()->put("add-to-cart.id.$count", $request->get('id'));
            $request->session()->put("add-to-cart.qty.".$request->get('id'), $id_count);

            if($id_count==1){

                $info = $this->get_item_data($this->getAWSItemSearchData(AwsProduct::getAWSItemLookup($request->get('id'))));

                $request->session()->put("add-to-cart.title.".$request->get('id'), $info['title'][0]);
                $request->session()->put("add-to-cart.link.".$request->get('id'), $info['link'][0]);
                $request->session()->put("add-to-cart.image.".$request->get('id'), $info['mediumImage'][0]?$info['mediumImage'][0]:''); 

                if(isset($info['ItemAttributes'][0]['PackageDimensions'])){

                    $dimensions = (array)$info['ItemAttributes'][0]['PackageDimensions'];
                     foreach($dimensions as $key=>$dimension){
                        //echo $key.'===>'.$dimension;
                         $request->session()->put("add-to-cart.attributes.".$request->get('id').'.'.$key, $dimension); 
                     }

                }
               
                $price = (array)$info['price'][0];
                //echo $price['FormattedPrice'];
                if(count($price)){
                    $request->session()->put("add-to-cart.price.".$request->get('id'), $price['FormattedPrice']);
                }else{
                   $request->session()->put("add-to-cart.price.".$request->get('id'), ''); 
                }
            }
        endif;

        $total_count = $this->getAddToCartTotal($request->session()->get('add-to-cart'));
        $request->session()->put("add-to-cart.total_qty", $total_count);
        //print_r( Session::get('add-to-cart'));
        return $total_count;        
    }


    /**
     * Display a Search of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAddToCartTotal($data)
    {  
        $total_count=0;
        foreach ($data['qty'] as $key => $items) {
            $total_count+=$items;
        }
        return $total_count;     
    }    


    /**
     * Display a Search of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {   
        //$url = $request->input('keywords')?$request->input('keywords'):$request->session()->get('keywords')?$request->session()->get('keywords'):Cookie::get('keywords');

        $url = $request->input('keywords')?$request->input('keywords'):$request->session()->get('keywords');

       
        if($url):
            $request->session()->put('keywords', $url);
            //Cookie::make('keywords', $url, 60);

            $status = AwsProduct::getUrlData($url);
            $page = $request->input('page')?$request->input('page'):1;

            if($status=='itemSearch'):
                for($i=$page; $i<=$page+1; $i++){
                    try {
                        $data[] = $this->get_item_data($this->getAWSItemSearchData(AwsProduct::getItemSearch($url, $i)));              
                    } catch (Exception $e) {
                       dd('Server error'); 
                    }
                    
                }
                return view('public.products.product-list', compact('data','url','page') );
            elseif($status!='invalid'):
              
                $records = $this->getAWSItemSearchData(AwsProduct::getAWSItemLookup($status));
                $data = $this->get_item_data($records);

                if($this->error){
                    return redirect('/')->withErrors($this->error_message);
                    die();
                }
                return view('public.products.product-detail', compact('data','url') );
            else:
                $data = $this->get_remote_data($url); 
                // GET request 
                //echo get_remote_data('http://example.com', "var2=something&var3=blabla" ); // POST request
                return view('public.products.product-detail-other', compact('data','url') );
            endif;
        else:
            return back()->withErrors($this->error_message);
            //return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;
    }


    /**
     * Display a Product detail of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        if($id):
            $data = $this->get_item_data($this->getAWSItemSearchData(AwsProduct::getAWSItemLookup($id)));
            if(isset($data['link'])):
                $url = $data['link'][0];
            else:
                $url = '';
            endif;    
            return view('public.products.product-detail', compact('data','url') );
        else:
              return redirect()->action('StaticPageController@home')->withErrors('Please try again....');
        endif;
    }



     public function getAWSItemSearchData($url)
    {
        
        if($url):
            $ch = curl_init();  
            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $output = curl_exec($ch);
            curl_close($ch);
            $this->xml = simplexml_load_string($output);
            return $this;
        else:
            return array();
        endif;
    }



    public function check_for_errors()
    {
        if(isset($this->xml->Error)) {
            $this->error_message = $this->xml->Error->Message;
            $this->error = 1;
        }
        if(isset($this->xml->Items->Request->Errors)) {
            $this->error_message = $this->xml->Items->Request->Errors->Error->Message;
            $this->error = 1;
        }
        return $this->error;
    }


    public function get_item_data()
    {
        if($this->check_for_errors()) return null;

        $item = array();
        try {

            if($this->xml){
                $items = $this->xml->Items->Item;
                foreach ($items as $key => $product) {

                    $item_id = $item['ASIN'][] = AwsProduct::getFormat((array)$product->ASIN);
                    $item['link'][] = AwsProduct::getAWSLink($item_id);
                    $item['title'][] = AwsProduct::getFormat((array)$product->ItemAttributes->Title);
                    $item['smallImage'][] = AwsProduct::getFormat((array)$product->SmallImage->URL);
                    $item['mediumImage'][] = AwsProduct::getFormat((array)$product->MediumImage->URL);
                    $item['largeImage'][] = AwsProduct::getFormat((array)$product->LargeImage->URL);
                    $item['price'][] = AwsProduct::getFormat((array)$product->OfferSummary);
                    $item['ImageSets'][] = AwsProduct::getFormat((array)$product->ImageSets);
                    $item['ItemAttributes'][] = (array)$product->ItemAttributes;
                    $item['OfferSummary'][] = (array)$product->OfferSummary;
                    $item['Offers'][] = (array)$product->Offers;
                }
            }     
        } catch (Exception $e) {
            
        }
        
        return $item;
    }


    public function get_remote_data($url, $post_paramtrs=false)
    {

    //See Updates and explanation at: https://github.com/tazotodua/useful-php-scripts/
    
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        if($post_paramtrs)
        {
            curl_setopt($c, CURLOPT_POST,TRUE);
            curl_setopt($c, CURLOPT_POSTFIELDS, "var1=bla&".$post_paramtrs );
        }
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
        curl_setopt($c, CURLOPT_COOKIE, 'CookieName1=Value;');
        curl_setopt($c, CURLOPT_MAXREDIRS, 10);
        $follow_allowed= ( ini_get('open_basedir') || ini_get('safe_mode')) ? false:true;
        if ($follow_allowed)
        {
            curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        }
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
        curl_setopt($c, CURLOPT_REFERER, $url);
        curl_setopt($c, CURLOPT_TIMEOUT, 60);
        curl_setopt($c, CURLOPT_AUTOREFERER, true);
        curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');

        $data = curl_exec($c);

        $status = curl_getinfo($c);
        curl_close($c);

        preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si',  $status['url'], $link); 
        $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si','$1=$2'.$link[0].'$3$4$5', $data);  
        $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si','$1=$2'.$link[1].'://'.$link[3].'$3$4$5', $data);


        if($status['http_code']==200)
        {
            return $data;
        }
        elseif($status['http_code']==301 || $status['http_code']==302)
        {
            if (!$follow_allowed)
            {
                if (!empty($status['redirect_url']))
                {
                    $redirURL = $status['redirect_url'];
                }
                else
                {
                    preg_match('/href\=\"(.*?)\"/si',$data,$m);
                    if (!empty($m[1]))
                    {
                        $redirURL = $m[1];
                    }
                }
                if(!empty($redirURL))
                {
                    return  call_user_func( __FUNCTION__, $redirURL, $post_paramtrs);
                }
            }
        }
        return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:".json_encode($status)."<br/><br/>Last data got<br/>:$data";
    }

}
