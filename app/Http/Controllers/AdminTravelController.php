<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminTravelController extends Controller
{
    public function index(){
        $travels = \App\Travel::paginate(10);
        // return $travels;
        return view('admin.pages.travels.index', compact('travels'));
    }
}
