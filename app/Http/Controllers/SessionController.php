<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function __construct(){
        $this->middleware('guest')->except(['destroy']);
    }
    public function destroy(){
    	Auth()->logout();
    	return redirect('/login');
    }

    public function create(){
    	return view('session/create');
    }

    public function store(){

        $this->validate(request(), [
            'email' => 'email|required',
            'password' => 'required'
            ]);

        if( ! auth()->attempt(['email' => request('email'), 'password' => request('password')]) ){
            return back()->withErrors('Check Your Credintials');
        }

    	return redirect()->home();
    }
}
