<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminOrderProductController extends Controller
{
    public function index(){
    	$products = \App\OrderProduct::paginate(10);
    	return view('admin.pages.order-products.index', compact('products'));
    }
}
