<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminOrderController extends Controller
{

     private $validate_rules = [
            'name' => 'required|max:255',
            'role' => 'required',
            'email' => 'required|email|unique:orders',
            'password' => 'required|confirmed',
    ];

    private $validate_rules_update = [
            'name' => 'required|max:255',
            'role' => 'required',
            'email' => 'required|email',
    ];

    public function index(){
        $orders = \App\Order::paginate(10);
        return view('admin.pages.orders.index', compact('orders'));
    }

    public function search(Request $request)
    {
    
        $keyword = $request->keyword;
        $search_fields = \App\Order::$search_fields;
        $result =   new \App\Order;
        
        if(request('keyword')){

            if(count($search_fields)){
                foreach ($search_fields as $field) {

                    $result = $result->orWhere( $field, 'like', '%' . $keyword . '%');

                    if($request->delivery_status == '0' || $request->delivery_status){
                        $result = $result->where('delivery_status', $request->delivery_status);
                    }

                    if($request->payment_status == '0' || $request->payment_status){
                        $result = $result->where('payment_status', $request->payment_status);
                    }

                    // if(request('message_type')){
                    //     $result = $result->whereHas('coupon', function ($query) use ($request) {
                    //          $query->where('message_type', $request->message_type);
                    //     });
                    // }
        
                }
            }

        } else {


            if($request->delivery_status == '0' || $request->delivery_status){
                $result = $result->where('delivery_status', $request->delivery_status);
            }

            if($request->payment_status == '0' || $request->payment_status){
                $result = $result->where('payment_status', $request->payment_status);
            }


            // if(request('coupon_text')){
            //     $result = $result->whereHas('coupon', function ($query) use ($request) {
            //         $query->where('coupon_text', 'like', "%{$request->coupon_text}%");
            //     });
            // }


        }



        return view('admin.pages.orders.index', ['orders'=> $result->paginate(10)]);
    }

    public function show($id){
        $order = \App\Order::find($id);
        return view('admin.pages.orders.show', compact('order'));
    }

    public function edit($id){
        $order = \App\Order::find($id);
        return view('admin.pages.orders.edit', compact('order'));
    }

    public function create(){
        return view('admin.pages.orders.create');
    }

    public function update($id, Request $request){
        $order = \App\Order::find($id);
        // $this->validate($request, $this->validate_rules_update);
        $post_data = request()->all();
        $update = $order->update($post_data);

        if($update){
            return back()->with('message', 'User Updated Succesfully');
        }
    }

    public function destroy($id){
        $delete = \App\Order::find($id)->delete();
        if($delete){
            return back()->with('message', 'User Deleted Succesfully');
        }
    }

    public function products($id){
    	$products = \App\OrderProduct::where('order_id', $id)->paginate(10);
        return view('admin.pages.order-products.index', compact('products'));
    }

}
