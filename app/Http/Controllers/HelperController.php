<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelperController extends Controller
{
    public static $front_settings = [
    	'css' => [
			'//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
			'//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf',
			'//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css',
			'//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css',
    		'/css/step-progressbar.css',
    		'/css/bootstrap.min.css',
			'/css/style.css',
			'/css/responsive.css'
    	],
    	'js' => [
    		'/js/jquery.js',
			'/js/bootstrap.min.js',
			'/js/cart.js',
			'//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js',
			'//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
			'/js/image-scale.js',
			'/js/app.js',
			'/js/carry.js',
    	]
    ];


    public static $admin_settings = [
    	'css' => [
			'/backend/css/bootstrap.min.css',
			'/backend/css/font-awesome.min.css',
			'//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css',
			'/backend/css/datepicker3.css',
			'/backend/css/styles.css',
			'/backend/css/style-admin.css',
    	],
    	'js' => [
    		'/backend/js/jquery-1.11.1.min.js',
			'/backend/js/bootstrap.min.js',
			'/backend/js/chart.min.js',
			'/backend/js/chart-data.js',
			'/backend/js/easypiechart.js',
			'/backend/js/easypiechart-data.js',
			'/backend/js/bootstrap-datepicker.js',
			'//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
			'/backend/js/custom.js',
			'/backend/js/app-admin.js',
    	]
    ];


}
