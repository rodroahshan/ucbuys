<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TravelController extends Controller
{
    

    public function my_travels(){
    	$user_id = auth()->user()->id;
    	$travels = \App\Travel::where('user_id', $user_id)->latest()->paginate(5);
        //return $travels;
    	//dd($travels);
    	return view('dashboard.pages.travels.my_travels', compact('travels'));
    }

    public function my_travel_details($travel_id){
        $travel = \App\Travel::find($travel_id);
        $products = \App\OrderProduct::all();
    	return view('dashboard.pages.travels.my_travel_details', compact('travel', 'products'));
    }

    public function create(){
    	return view('dashboard.pages.travels.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'country_from' => 'required',
            'country_to'   => 'required',
            'luckege_qty'  => 'required',
            'travel_date'  => 'required'
            ]);
        //dd(md5($request->password));
        //dd(request()->all());
        $user = 
            \App\Travel::create([
            'user_id' => auth()->id(),
            'country_from' => $request->country_from,
            'country_to' => $request->country_to,
            'luckege_qty' => $request->luckege_qty,
            'travel_date' => $request->travel_date,   
            ]);

        return back()->withInput()->withErrors('Success');

    }
}
