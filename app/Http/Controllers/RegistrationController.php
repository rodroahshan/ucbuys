<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function __construct(){
        $this->middleware('guest')->only(['create', 'store']);
    }
    public function create(){
    	return view('registration.create');
    }
    public function store(){
    	$this->validate(request(), [
    		'name' => 'required',
    		'email' => 'required|email|unique:users',
    		'password' => 'required|confirmed'
    		]);
    	$user = \App\User::create([
    		'name' => request('name'),
    		'email' => request('email'),
    		'password' => bcrypt(request('password')),
    		]);
    	//return $user;
    	auth()->login($user);
    	return redirect()->home();

    }
}
