<?php 
function pr($print, $die=false){
	echo "<pre>";
	print_r($print);
	echo "</pre>";
	if($die) die();
}

function on_bdt($amount, $raw=false){
	if(!$amount){
		return '৳' . 0;
	}
	return '৳' . number_format($amount, 2);
}

function formated_date($date){
	return \Carbon\Carbon::parse($date)->toFormattedDateString();
}

function myText($title, $field, $value, $attr){
	$out = '';
	$out .= "<div class='form-group'>";
	$out .= "<label class='col-md-2 control-label'>{$title}</label>";
	$out .= '<div class="col-md-10">';
	$out .= Form::text($field, $value, $attr);
	$out .= '</div>';
	$out .= '</div>';

	return $out;
}

function mySubmit($title, $attr){
	$out = '';
	$out .= "<div class='form-group'>";
	$out .= "<label class='col-md-2 control-label'></label>";
	$out .= '<div class="col-md-10">';
	$out .=  Form::submit($title, $attr);
	$out .= '</div>';
	$out .= '</div>';

	return $out;
}

function myTextArea($title, $field, $value, $attr){
	$out = '';
	$out .= "<div class='form-group'>";
	$out .= "<label class='col-md-2 control-label'>{$title}</label>";
	$out .= '<div class="col-md-10">';
	$out .= Form::textarea($field, $value, $attr);
	$out .= '</div>';
	$out .= '</div>';

	return $out;
}


function myEmail($title, $field, $value, $attr){
	$out = '';
	$out .= "<div class='form-group'>";
	$out .= "<label class='col-md-2 control-label'>{$title}</label>";
	$out .= '<div class="col-md-10">';
	$out .= Form::email($field, $value, $attr);
	$out .= '</div>';
	$out .= '</div>';

	return $out;
}

function mySelect($title, $field, $data, $value, $attr){
	$out = '';
	$out .= "<div class='form-group'>";
	$out .= "<label class='col-md-2 control-label'>{$title}</label>";
	$out .= '<div class="col-md-10">';
	$out .= Form::select($field, $data, $value, $attr);
	$out .= '</div>';
	$out .= '</div>';
	return $out;
}

function myPassword($title, $field, $attr){
	$out = '';
	$out .= "<div class='form-group'>";
	$out .= "<label class='col-md-2 control-label'>{$title}</label>";
	$out .= '<div class="col-md-10">';
	$out .= Form::password($field, $attr);
	$out .= '</div>';
	$out .= '</div>';

	return $out;
}



?>
