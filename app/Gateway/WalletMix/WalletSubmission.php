<?php
/*
	Author: Walletmix Ltd.
	Version: 2.5.4.17
	Title: Walletmix Payment Gateway Integration (data submission)
	Email: support@walletmix.com

*/
namespace App\Gateway\WalletMix;	
use App\Gateway\WalletMix\WALLETMIX as WALLETMIX;

class WalletSubmission{

	public $walletmix;

	private $access_username = 'technohaat_755515762';
	private $access_password = 'technohaat_610477812';
	private $merchant_id = 'WMX5bf2a427c58f9';
	private $access_app_key = 'e806569417fa14121b32cde42a5c9829576db6f0';


	private $domain_name = "technohaat.com";
	// private $domain_name = "bdboom.com";
	private $callback_url;
	public $errors = [];

	function __construct(){
		$this->callback_url = url('/order/confirm_payment');
		$this->walletmix = NEW WALLETMIX($this->access_username, $this->access_password, $this->merchant_id, $this->access_app_key);
	}


	public function call_back_walletmix(){
		// dd($_POST);
		$this->walletmix->set_database_driver('session');	// options: "txt" or "session"

			if(isset($_POST['merchant_txn_data'])){
				$merchant_txn_data = json_decode($_POST['merchant_txn_data']);
				
				$this->walletmix->get_database_driver();
				
				if($this->walletmix->get_database_driver() == 'txt'){
					$saved_data = json_decode($this->walletmix->read_file());
				}elseif($this->walletmix->get_database_driver() == 'session'){
					// Read data from your database
					$saved_data = json_decode($this->walletmix->read_data());
				}
				
				if($merchant_txn_data->token === $saved_data->token){
					
					$wmx_response = json_decode($this->walletmix->check_payment($saved_data));
					//$this->walletmix->debug($wmx_response,true);
					if(	($wmx_response->wmx_id == $saved_data->wmx_id) ){
						if(	($wmx_response->txn_status == '1000') ){
							if(	($wmx_response->bank_amount_bdt >= $saved_data->amount) ){
								if(	($wmx_response->merchant_amount_bdt == $saved_data->amount) ){	
									// -- SUCCESS -- //
									return array(
										'trans' => true,
										'success' => true,
										'response' => $wmx_response,
										'message' => 'Your order has been placed successfully.'
									);
									//echo 'Update merchant database with success. amount : '.$wmx_response->bank_amount_bdt;
								}else{
									return array(
										'trans' => true,
										'success' => false,
										'response' => $wmx_response,
										'message' => 'Your payment amount is not mitcmatch with your order amount.'
									);
									//echo 'Merchant amount mismatch Merchant Amount : '.$saved_data->amount.' Bank Amount : '.$wmx_response->bank_amount_bdt.'. Update merchant database with success';
								}
							}else{
								return array(
										'trans' => true,
										'success' => false,
										'response' => $wmx_response,
										'message' => 'Bank amount is less then merchant amount'
									);
								//echo 'Bank amount is less then merchant amount like partial payment.You can make it failed transaction.';
							}
						}else{
							return array(
								'trans' => false,
								'success' => false,
								'response' => $wmx_response,
								'message' => 'Your payment has not verified or cencelled'
							);
							//echo 'Update merchant database with failed';
						}
					}else{
						return array(
							'trans' => false,
							'success' => false,
							'response' => $wmx_response,
							'message' => 'Merchant ID Mismatch'
						);
						//echo 'Merchant ID Mismatch';
					}
				}else{
					return array(
						'trans' => false,
						'success' => false,
						'response' => $wmx_response,
						'message' => 'Token mismatch'
					);
					//echo 'Token mismatch';
				}
			}else{
				return array(
					'trans' => false,
					'success' => false,
					'response' => $wmx_response,
					'message' => 'Try to direct access'
				);
				//echo 'Try to direct access';
			}


		}


	// $customer_info = array(
	// 	"customer_name" 	=> "Michel Schofield",
	// 	"customer_email" 	=> 'schofield@gmail.com',
	// 	"customer_add" 		=> "Nikunjo",
	// 	"customer_city" 	=> "Dhaka",
	// 	"customer_country" 	=> "Bangladesh",
	// 	"customer_postcode" => "1229",
	// 	"customer_phone" 	=> "01910000000",
	// );
	// $shipping_info = array(
	// 	"shipping_name" => "Linking Borrows",
	// 	"shipping_add" => "1600 Amphitheatre Pkwy, Mountain View",
	// 	"shipping_city" => "New York City",
	// 	"shipping_country" => "United States",
	// 	"shipping_postCode" => "1600",
	// );

	public function submit_to_walletMix($order_id, $products, $discount, $shipping_charge, $customer_info, $shipping_info){
		$this->submit($order_id, $products, $discount, $shipping_charge, $customer_info, $shipping_info);
	}

	public function submit($order_id, $products, $discount=0, $shipping_charge=0, $customer_info, $shipping_info){

		
		// For TESTING
		// $discount=0; $shipping_charge=0;
		// $product_1 = array('name' => 'Adata 16GB Pendrive','price' => 5,'quantity' => 1);
		// $product_2 = array('name' => 'Adata 8GB Pendrive','price' => 5,'quantity' => 1);
		// $products = array($product_1,$product_2);


		$this->walletmix->set_shipping_charge($shipping_charge);
		$this->walletmix->set_discount($discount);
		$this->walletmix->set_product_description($products);
		$this->walletmix->set_merchant_order_id($order_id);
		$this->walletmix->set_app_name($this->domain_name);
		$this->walletmix->set_currency('BDT');
		$this->walletmix->set_callback_url($this->callback_url);

		$extra_data = array();
		//if you want to send extra data then use this way
		//$extra_data = array('param_1' => 'data_1','param_2' => 'data_2','param_3' => 'data_3');
		$this->walletmix->set_extra_json($extra_data);

		$this->walletmix->set_transaction_related_params($customer_info);
		$this->walletmix->set_transaction_related_params($shipping_info);

		$this->walletmix->set_database_driver('session');	// options: "txt" or "session"

		//dd($this->walletmix);

		$this->walletmix->send_data_to_walletmix();
	}



	// public function submit(){
	// 	$customer_info = array(
	// 		"customer_name" 	=> "Michel Schofield",
	// 		"customer_email" 	=> 'schofield@gmail.com',
	// 		"customer_add" 		=> "Nikunjo",
	// 		"customer_city" 	=> "Dhaka",
	// 		"customer_country" 	=> "Bangladesh",
	// 		"customer_postcode" => "1229",
	// 		"customer_phone" 	=> "01910000000",
	// 	);
	// 	$shipping_info = array(
	// 		"shipping_name" => "Linking Borrows",
	// 		"shipping_add" => "1600 Amphitheatre Pkwy, Mountain View",
	// 		"shipping_city" => "New York City",
	// 		"shipping_country" => "United States",
	// 		"shipping_postCode" => "1600",
	// 	);

	// 	$this->walletmix->set_shipping_charge(0);
	// 	$this->walletmix->set_discount(0);

	// 	$product_1 = array('name' => 'Adata 16GB Pendrive','price' => 5,'quantity' => 2);
	// 	$product_2 = array('name' => 'Adata 8GB Pendrive','price' => 5,'quantity' => 1);

	// 	$products = array($product_1,$product_2);

	// 	$this->walletmix->set_product_description($products);

	// 	$this->walletmix->set_merchant_order_id(555);

	// 	$this->walletmix->set_app_name($this->domain_name);
	// 	$this->walletmix->set_currency('BDT');
	// 	$this->walletmix->set_callback_url($this->callback_url);

	// 	$extra_data = array();

	// 	//if you want to send extra data then use this way
	// 	//$extra_data = array('param_1' => 'data_1','param_2' => 'data_2','param_3' => 'data_3');

	// 	$this->walletmix->set_extra_json($extra_data);

	// 	$this->walletmix->set_transaction_related_params($customer_info);
	// 	$this->walletmix->set_transaction_related_params($shipping_info);

	// 	$this->walletmix->set_database_driver('txt');	// options: "txt" or "session"

	// 	//dd($this->walletmix);

	// 	$this->walletmix->send_data_to_walletmix();
	// }
	
	
}



?>