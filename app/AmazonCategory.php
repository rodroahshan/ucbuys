<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class AmazonCategory extends Model
{

	public static function random_category_name(){
		$random_section_array = ['category', 'item'];
		$random_section = self::random_value_from_array($random_section_array);

		$categories = self::$fasion;
		if($random_section == 'category'){
			$random_category_key = array_rand($categories);
			$random_keyword = $categories[$random_category_key]['name'];
		}

		if($random_section == 'item'){
			$random_category = array_rand($categories);
			$random_item_array = $categories[$random_category]['items'];
			$random_keyword = self::random_value_from_array($random_item_array);
		}

		return $random_keyword;

	}

	public static function random_value_from_array($array){
		$random_key = array_rand($array);
		return $array[$random_key];
	}
	
    public static $fasion = array(
		'top_wear' => 
			array(
				'name' => 'Top Wear',
				'items' => 
					array(
						'T-Shirts',
						'Casual Shirts',
						'Suits',
						'Formal Shirts',
						'Hoodies',
						'Jackets',
						'Sweaters &amp; Cardigans',
						'Sweat Shirt',
						'Blazer &amp; Coats',
						'Polo Shirts',
						'Waist Coats',
						'Sunglasses',
					)	
			),
		'bottom_wear' => 
			array(
				'name' => 'Bottom Wear',
				'items' => 
					array(
						'Formal Pants',
						'Jeans',
						'Sports &amp; Bermudas',
						'Trousers',
						'Denim Pant',
						'Chinos Pant',
						'Gabardine Pant',
					)	
			),
		'traditional_wear' => 
			array(
				'name' => 'Traditional Wear',
				'items' => 
					array(
						'Panjabi',
						'Pajama',
					)	
			),	
		'accessories' => 
			array(
				'name' => 'Accessories',
				'items' => 
					array(
						'Bags &amp; Backpacks',
						'Cufflinks',
						'Belts',
						'Beads &amp; Bracelets',
					)	
			),
		'foot_wear' => 
			array(
				'name' => 'Foot Wear',
				'items' => 
					array(
						'Casual Shoes',
						'Athletic',
						'Boots',
						'Dress Shoes',
						'Sandals &amp; Flip-flops',
						'Socks',
					)	
			),	
		'inner_wear' => 
			array(
				'name' => 'Inner Wear',
				'items' => 
					array(
						'Underwear',
					)	
			),
		'fragrance' => 
			array(
				'name' => 'Fragrance',
				'items' => 
					array(
						'Perfume',
					)	
			),
		'top_wear' => 
			array(
				'name' => 'Top Wear',
				'items' => 
					array(
						'Sweater',
						'Hoodies',
						'Coats &amp; Jackets',
						'Tunic',
						'Shrugs',
						'Tops',
						'Dress',
						'Kimono',
						'Shirts',
					)	
			),
		'bottom_wear' => 
			array(
				'name' => 'Bottom Wear',
				'items' => 
					array(
						'Leggings',
						'Pants',
						'Palazzo',
					)	
			),
		'traditional_wear' => 
			array(
				'name' => 'Traditional Wear',
				'items' => 
					array(
						'Kurti &amp; Fatua',
						'Salwar Kameez',
						'Boutique Dress',
						'Saree',
						'Unstitched Dress',
						'Gown',
						'Lehenga',
						'Anarkali Suits',
					)	
			),
		'seasonal_wear' => 
			array(
				'name' => 'Seasonal Wear',
				'items' => 
					array(
						'Shawl',
					)	
			),	
		'footwear' => 
			array(
				'name' => 'Footwear',
				'items' => 
					array(
						'Flats &amp; Sandals',
						'Pumps &amp; Heels',
						'Casual',
						'Sneakers',
						'Socks',
					)	
			),
		'handbags_bags_wallets' => 
			array(
				'name' => 'Handbags, Bags & Wallets',
				'items' => 
					array(
						'Handbags &amp; Totes',
						'Backpacks',
						'Clutches',
					)	
			),
		'fragrance' => 
			array(
				'name' => 'Fragrance',
				'items' => 
					array(
						'Perfume',
						'Body Mist',
						'Deodorants',
					)	
			),
		'modest_wear' => 
			array(
				'name' => 'Modest Wear',
				'items' => 
					array(
						'Gown',
						'Hijab',
					)	
			),
		'other' => 
			array(
				'name' => 'Other',
				'items' => 
					array(
						'Jewellery',
						'Night Suits',
					)	
			),	
		'face_makeup' => 
			array(
				'name' => 'Face Makeup',
				'items' => 
					array(
						'Foundations',
						'BB &amp; CC Cream',
						'Primers',
						'Concealer &amp; Contouring',
						'Highlighter &amp; Bronzers',
						'Blush',
						'Makeup Remover',
						'Compact &amp; Powders',
						'Make up setting sprays',
					)	
			),	
		'eye_makeup' => 
			array(
				'name' => 'Eye Makeup',
				'items' => 
					array(
						'Eye Brows Pencil &amp; Palettes',
						'Eye Liners &amp; kajal',
						'Eye Shadows &amp; Palettes',
						'Mascara',
					)	
			),	
		'hair_care' => 
			array(
				'name' => 'Hair Care',
				'items' => 
					array(
						'Hair Oil',
						'Hair Treatment',
						'Hair Shampoo',
					)	
			),
		'lip_makeup' => 
			array(
				'name' => 'LIP MAKEUP',
				'items' => 
					array(
						'Lipstick',
						'Lip gloss',
						'Lip Liner',
						'Liquid Lipsticks',
					)	
			),
		'makeup_accessories' => 
			array(
				'name' => 'Makeup Accessories',
				'items' => 
					array(
						'Beauty Blender &amp; ',
						'Brush Set',
						'Lip Brushes',
					)	
			),	
		'body_skin_care' => 
			array(
				'name' => 'Body & Skin Care',
				'items' => 
					array(
						'Essential Oil',
						'Face Mask',
						'Face Wash',
						'Day Cream',
						'Toner',
						'Moisturizing Cream',
						'Night Cream',
						'Anti-Aging Skin Care',
						'Aging Eye Cream',
						'Eye Gel',
						'Body Scrub',
						'Body Butter',
						'Moisturizing Lotion',
						'Beauty Care',
					)	
			),	
		'nails' => 
			array(
				'name' => 'NAILS',
				'items' => 
					array(
						'Nail Polish',
						'Nail polish removers',
					)	
			),	
		
	);
}
