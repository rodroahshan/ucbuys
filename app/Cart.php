<?php

namespace App;


class Cart extends Model
{
    public function __construct(){
        $cart = ['items' => []];
        $this->cart = session('cart') ? session('cart') : $cart;

        if(session('cart')){
            // $this->grand_net_price = get_grand_net_price();
            // $this->grand_shipping_charge = get_grand_shipping_charge();
            // $this->grand_bdboom_charge = get_grand_bdboom_charge();
            // $this->grand_total = get_grand_total();


            // $this->sub_total = $this->get_sub_total();
            // $this->grand_total = $this->get_grand_total();
            // $this->net_qty = $this->net_count_from_sesion();
        }
        
    }

	public $cart;
    public $sub_total;
    // public $grand_total;
    public $delivery_fee = 0;
    public $discount = 0;
    public $net_qty = 0;


    // public $grand_net_price = 0;
    // public $grand_shipping_charge = 0;
    // public $grand_bdboom_charge = 0;
    // public $grand_total = 0;

    public static function get_total_cart($on_bdt = false){

        $grand_net_price = 0;
        $grand_shipping_charge = 0;
        $grand_bdboom_charge = 0;
        $grand_total = 0;
        $grand_qty = 0;
        $discount = \App\Order::get_discount();
        $delivery_charge = \App\Order::get_delivery_charge();
        $gateway_charge = 0;
        $all_total = 0;
        $master_total = 0;

        if($cart = session('cart')){

            foreach ($cart['items'] as $item => $key) {
                $grand_net_price += $key['net_price'] * $key['qty'];
                $grand_shipping_charge += $key['shipping_charge'] * $key['qty'];
                $grand_bdboom_charge += $key['bdboom_charge'] * $key['qty'];
                $grand_total += $key['total'] * $key['qty'];
                $grand_qty += $key['qty'];
            }

            $all_total = $grand_total + $delivery_charge - $discount;
            $gateway_charge = \App\Order::get_gateway_charge($all_total);
            $master_total = $all_total + $gateway_charge;

            $total_cart =  [
                'grand_qty' => $grand_qty,
                'grand_net_price' => $on_bdt ? on_bdt(round($grand_net_price, 2)) : round($grand_net_price, 2),
                'grand_shipping_charge' => $on_bdt ? on_bdt(round($grand_shipping_charge, 2)) : round($grand_shipping_charge, 2),
                'grand_bdboom_charge' => $on_bdt ? on_bdt(round($grand_bdboom_charge, 2)) : round($grand_bdboom_charge, 2),
                'grand_total' => $on_bdt ? on_bdt(round($grand_total, 2)) : round($grand_total, 2),
                'discount' => $on_bdt ? on_bdt(round($discount, 2)) : round($discount, 2),
                'delivery_charge' => $on_bdt ? on_bdt(round($delivery_charge, 2)) : round($delivery_charge, 2),
                'gateway_charge' => $on_bdt ? on_bdt(round($gateway_charge, 2)) : round($gateway_charge, 2),
                'all_total' => $on_bdt ? on_bdt(round($all_total, 2)) : round($all_total, 2),
                'master_total' => $on_bdt ? on_bdt(round($master_total, 2)) : round($master_total, 2),
            ];
            // dd($total_cart);
            return $total_cart;
        }
    }

    public static function cart_items(){
        $cart_items = session('cart') ? session('cart')['items'] : [];
        return $cart_items;
    }

    public static function get_grand_qty(){
        $total_cart = self::get_total_cart();
        return $total_cart['grand_qty'];
    }






    public function products(){
        $products = [];
        foreach ($this->cart['items'] as $key => $item){
            $product = array(
                    'name' => $item['title'],
                    'price' => $item['total'],
                    'quantity' => $item['qty'],
                );
            $products[] = $product;
        }
        // $product_1 = array('name' => 'Adata 16GB Pendrive','price' => 5,'quantity' => 2);
        // $product_2 = array('name' => 'Adata 8GB Pendrive','price' => 5,'quantity' => 1);
        // $products = array($product_1,$product_2);
        return $products;
        //dd($this->cart['items']);
    }


    public function productAddToCart($id){
        //if(session('cart')){ $this->cart = session('cart'); }

    	if(!array_key_exists($id, $this->cart['items'])){ // not added
            //dd('not added');
			$product = \App\Amazon::item($id, $session=true);
            $this->cart['items'][$id] = $product;
        	$this->cart['items'][$id]['qty'] = 1;
    	} else {
            //dd('added');
    		$this->cart['items'][$id]['qty'] += 1;
    	}

        session([ 'cart' => $this->cart ]);

        return true;
    }

    // public function net_count_from_sesion($output='count'){
    //     $netCount = 0;
    //     $netPrice = 0;

    //     if($cart = session('cart')){

    //         foreach ($cart['items'] as $item => $key) {
    //             (int) $netCount += (int) $key['qty'];
    //             (int) $netPrice += (int) $key['total'] * (int) $key['qty'];
    //         }
    //     }

    //     if($output == 'count'){
    //         return $netCount;
    //     } elseif($output == 'price') {
    //         return $netPrice;
    //     }
    // }

    // public function get_sub_total(){
    //     return $this->net_count_from_sesion('price');
    // }

    // public function get_grand_total(){
    //     return $this->get_sub_total() + $this->shipping;
    // }


    public static function get_net_qty_by_id_from_session($id){
        if($cart = session('cart')){
            return $cart['items'][$id]['qty'];
        }

    }

    public function plus_item($id){
        $cart = session('cart');
        if($cart){
            $cart['items'][$id]['qty'] += 1;
        }
        session([ 'cart' => $cart ]);
        return true;
    }

    public function minus_item($id){
        $cart = session('cart');
        if($cart){
            if($cart['items'][$id]['qty'] <= 1){
                unset($cart['items'][$id]);
            } else {
                $cart['items'][$id]['qty'] -= 1;
            }
        }
        session([ 'cart' => $cart ]);
        return true;
    }


    public function remove_item($id){
        $cart = session('cart');
        if($cart){
            unset($cart['items'][$id]);
        }
        session([ 'cart' => $cart ]);
        return true;
    }

    public function resetCart(){
        if(session('cart')){
            session()->forget('cart');
        }
        return true;
    }

    public function resetAll(){
        session()->flush();
        return true;
    }
}
