<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


define("GOOGLEAPIKEY", "AIzaSyDGklCsiewCxzVB_qLeT9yB7fIpglM7P2E");


class AwsProductCheckout extends Model
{

 	public static function getAddressToLatLong($address)
    {
    	// Get lat and long by address         
        $prepAddr = str_replace(' ','+',$address);// Google HQ
        $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=true_or_false&key='.GOOGLEAPIKEY);
        $output= json_decode($geocode);
        //print_r($output);
        if($output){
        	$latitude = $output->results[0]->geometry->location->lat;
        	$longitude = $output->results[0]->geometry->location->lng;      
        	$lat = array('lat'=>$latitude,'long'=>$longitude);
        }else{
        	$lat = array('lat'=>23.810332,'long'=>90.4125181);
        }

        return self::getLatLongToMap($lat);
               
    }


    public static function getLatLongToMap($geocode)
    {    
    ?>	
    	<script src='http://maps.googleapis.com/maps/api/js?libraries=places&key='.GOOGLEAPIKEY></script>
		<script>
		    var myCenter = new google.maps.LatLng(<?php echo $geocode['lat'];?>, <?php echo $geocode['long'];?>);
		    function initialize() {
		        var mapProp = {
		            center:myCenter,
		            zoom:16,
		            scrollwheel:false,
		            draggable:false,
		            mapTypeId:google.maps.MapTypeId.ROADMAP
		        };
		    
		        var map = new google.maps.Map(document.getElementById("map"),mapProp);
		        
		        var marker = new google.maps.Marker({
		            position:myCenter,
		        });
		        marker.setMap(map);
		    }
		    google.maps.event.addDomListener(window, 'load', initialize);
		</script>



    <?php
    }


    public static function getParcelDeliveryFees($fees)
    {
    	return $fees.' BDT';
               
    }


    public static function getDeliveryTo($data)
    {
    		
		return self::getDeliveryToCity($data).self::getDeliveryToCountry($data);          
    }

    public static function getDeliveryToCountry($data)
    {
    	if($data['country_id']):
	    	return DB::table('countries')
	    	->where('id', $data['country_id'])
	    	->first()->name;
	    else:
	    	return;
	    endif;	
    }

    public static function getDeliveryToCity($data)
    {
    	if($data['city_id']):
	    	return DB::table('cities')
	    	->where('id', $data['city_id'])
	    	->first()->name.', ';
	    else:
	    	return;
	    endif;	           
    }

    public static function getPickupFrom($data, $app)
    {	
    	if($data['pickup_type']==1):
    		return $app->store_address;
        elseif($data['pickup_type']==2):
        	return 'Airposted Parcel Delivery';
        elseif($data['pickup_type']==3):		
        	return 'Traveler';
        endif;			         
    }


    public static function getDeliveryBefore($data)
    {		
		return date_format(date_create($data['deliver_date']),"F j, Y");
    }


    public static function getSubtotal($data)
    {	$sub_total = 0;	
    	if(isset($data['price'])):
	    	foreach ($data['price'] as $key => $price) {
	    		if($price){
	    		  //echo $key;
	    		  //echo self::getPrice($price).'---';
	    		  $sub_total+= self::getPrice($price);
	    		}	
	    	}
	    	return self::getCurrency($data['price'][$key]).number_format($sub_total, 2);
	    else:
	    	return 0;
	    endif;	
		
    }


    public static function getSubtotalNonFormat($data)
    {	$sub_total = 0;	
    	if(isset($data['price'])):
	    	foreach ($data['price'] as $key => $price) {
	    		if($price){
	    		  //echo $key;
	    		  //echo self::getPrice($price).'---';
	    		  $sub_total+= self::getPrice($price);
	    		}	
	    	}

	    	$currency = self::getCurrency($data['price'][$key]);
	    	return array('sub_total'=>$sub_total,'currency'=>$currency);
	    else:
	    	return 0;
	    endif;	
		
    }

    public static function getCurrency($price)
    {
      $price = explode(' ', $price);
      if(count($price)){
      	return $price[0].' ';
      }
      return 0;
    }


    public static function getPrice($price)
    {
      $price = explode(' ', $price);
      if(count($price)){
      	return (float)str_replace(',', '', $price[1]);
      }
      return 0;
    }	


    public static function getShippingFee($data, $app)
    {	
    	 $sub_total = self::getSubtotalNonFormat($data);
    	//$dollar_rate = $app->dollar_rate;
    	// $commission = $app->commission;
    	//$transaction_charge = $app->transaction_charge;
    	return $sub_total['currency'].' '.number_format(self::getCommission($sub_total['sub_total'], $app->commission?$app->commission:0),2);
    }

    public static function getCommission($total, $commission)
    {
    	
    	return (float)($commission/100)*$total;

    }	


    public static function getEstimatedTotal($data, $app)
    {	
    	 $sub_total = self::getSubtotalNonFormat($data);
    	 //$dollar_rate = $app->dollar_rate;
    	// $commission = $app->commission;
    	//$transaction_charge = $app->transaction_charge;
    	return $sub_total['currency'].' '.number_format($sub_total['sub_total'] + self::getCommission($sub_total['sub_total'], $app->commission?$app->commission:0),2);
    }


    

}
