$(function(){
	// $('#admin-nav a[href^="/' + location.pathname.split("/")[1] + '"]').parent('li').addClass('active');
    var current = location.pathname;
    //console.log(current);
    $('#admin-nav li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('path') == current){
            $this.parent('li').addClass('active');
        }
    })
});

$(document).ready(function(){
  $('.select2').select2();
});

// Delete Confirm js

$(document).ready(function(){
  $(".del").click(function(){
    if (!confirm("Do you want to delete")){
      return false;
    }
  });
});