jQuery(document).ready(function($) {

	$('.add_to_cart').on('click', function(event) {
		event.preventDefault();
		var self = $(this);
		var item_id = self.attr('item_id');
		self.text('Adding');
		$.ajax({
			url: base_url + '/add-to-cart',
			type: 'GET',
			dataType: 'json',
			data: {item_id: item_id, ajax : 1},
		})
		.done(function(data) {
			if(data){
               update_cart(data.cart);
               self.text('Add To Cart');
			}
		});
		
	});


	$('.plus_item').on('click', function(event) {
		event.preventDefault();
		var self = $(this);
		var item_id = self.attr('item_id');

		$.ajax({
			url: base_url + '/plus_item',
			type: 'GET',
			dataType: 'json',
			data: {item_id: item_id, ajax : 1},
		})
		.done(function(data) {
			if(data){
			   self.next('.tit-d-cnt').val(data.product_qty);
               update_cart(data.cart);
			}
		});
		
	});


	$('.minus_item').on('click', function(event) {
		event.preventDefault();
		var self = $(this);
		var item_id = self.attr('item_id');

		$.ajax({
			url: base_url + '/minus_item',
			type: 'GET',
			dataType: 'json',
			data: {item_id: item_id, ajax : 1},
		})
		.done(function(data) {
			if(data){
			   self.prev('.tit-d-cnt').val(data.product_qty);
               update_cart(data.cart);
			}
		});
		
	});

	$('.remove_item').on('click', function(event) {
		event.preventDefault();
		var self = $(this);
		var item_id = self.attr('item_id');

		$.ajax({
			url: base_url + '/remove_item',
			type: 'GET',
			dataType: 'json',
			data: {item_id: item_id, ajax : 1},
		})
		.done(function(data) {
			if(data){
			   self.parents('tr').remove();
               update_cart(data.cart);
			}
		});
		
	});


	




 
	function update_cart(cart){
		$('.cart-icon').text(cart.grand_qty);
		$('.grand_qty').text(cart.grand_qty);
		$('.grand_net_price').text(cart.grand_net_price);
		$('.grand_shipping_charge').text(cart.grand_shipping_charge);
		$('.grand_bdboom_charge').text(cart.grand_bdboom_charge);
		$('.grand_total').text(cart.grand_total);
		$('.all_total').text(cart.all_total);
		$('.gateway_charge').text(cart.gateway_charge);
		$('.master_total').text(cart.master_total);

		toastr.success("Cart Updated");
	}
});