$(document).ready(function(){
  $('.select2').select2();
});

//https://github.com/gestixi/image-scale
$(function() {
  $("img.scale").imageScale({
  	rescaleOnResize: true
  });
});

$(function() {
	// single products variation change then redirect to that page
    $('#single-product-variations').on('change', function(event) {
		event.preventDefault();
		var asin_id = $(this).val();
		// window.location.href = "//";
		if(asin_id){
			window.location = "/products/" + asin_id;
		}
	});
});



// Mega menu show when category hover
// $(document).ready(function(){
//   $('.maga-menu-item h3').on('click', function(){
//   	var self = $(this);
//   	self.next('ul').slideToggle();
//   });
// });