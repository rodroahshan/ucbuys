<?php



Route::get('/clear', function(){
	session()->flush();
    \Artisan::call('cache:clear');
    // \Artisan::call('route:cache');
    \Artisan::call('view:clear');
    \Artisan::call('config:clear');
	echo "session cleared";
});

Route::get('/pagination', 'ProductController@pagination');

Route::get('/migrate-refresh', function(){ \Artisan::call('migrate:refresh'); echo "Database Deleted and Created"; });

Route::get('/', 'ProductController@index')->name('home');
Route::get('/products/search', 'ProductController@search')->name('search');
Route::get('/products/cart', 'ProductController@cart');
Route::get('/products/checkout', 'ProductController@checkout');
Route::get('/products/payment', 'ProductController@payment');
Route::get('/products/{product}', 'ProductController@show');

// ORDER Place
Route::post('/order/confirm', 'OrderController@create');
Route::get('/order/{id}/pay_now', 'OrderController@pay_now');

// Payment Call Back
Route::post('/order/confirm_payment', 'OrderController@confirm_payment');


// CART
Route::get('/reset-cart', 'ProductController@resetCart');
Route::get('/reset-all', 'ProductController@resetAll');
Route::any('/add-to-cart', 'ProductController@addToCart');
Route::any('/plus_item', 'ProductController@plus_item');
Route::any('/minus_item', 'ProductController@minus_item');
Route::any('/remove_item', 'ProductController@remove_item');

// USER DASHBOARD

Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard/account', 'DashboardController@account');
// buyer
Route::get('/dashboard/my_orders', 'OrderController@my_orders');
Route::get('/dashboard/my_order_details/{order_id}', 'OrderController@my_order_details');

// traveler

Route::get('traveler', 'TravelerController@index');

Route::get('/dashboard/my_travels', 'TravelController@my_travels');
Route::get('/dashboard/my_travels/create', 'TravelController@create');
Route::post('/dashboard/my_travels/create', 'TravelController@store');
Route::get('/dashboard/my_travel_details/{travel_id}', 'TravelController@my_travel_details');




/** AUTH **/
Route::get('/auth/register', 'AuthController@register');
Route::post('/auth/register', 'AuthController@store');

Route::get('/auth/login', 'AuthController@login')->name('login');
Route::post('/auth/login', 'AuthController@login_check');


Route::get('/auth/login/facebook', 'AuthController@redirectToProvider');
Route::get('/auth/login/facebook/callback', 'AuthController@handleProviderCallback');

Route::get('/auth/logout', 'AuthController@logout');




Route::post('/user/update_user', 'UserController@update_user');
Route::post('/user/update_password', 'UserController@update_password');


/** Traveler **/
Route::get('/traveler/register', 'AuthController@traveler_register');

/** STATIC PAGES **/
Route::get('/about-us', 'StaticPageController@about_us');
Route::get('/faq', 'StaticPageController@faq');
Route::get('/contact-us', 'StaticPageController@contact_us');
Route::get('/how-it-works', 'StaticPageController@how_it_works');
Route::get('/why-us', 'StaticPageController@why_us');


/** Admin **/

Route::group(['middleware' => 'auth'], function(){

	Route::get('/admin/form', function(){
		return view('admin.pages.form');
	});

	Route::get('/admin/', function(){
		return view('admin.pages.home');
	});

	Route::get('/admin/orders', 'AdminOrderController@index');
	Route::get('/admin/orders/search', 'AdminOrderController@search');
	Route::get('/admin/orders/{id}', 'AdminOrderController@show');
	Route::get('/admin/orders/{id}/products', 'AdminOrderController@products');
	Route::get('/admin/orders/{id}/edit', 'AdminOrderController@edit');
	Route::put('/admin/orders/{id}', 'AdminOrderController@update');
	Route::get('/admin/orders/{id}/delete', 'AdminOrderController@destroy');


	Route::get('/admin/products', 'AdminOrderProductController@index');
	Route::get('/admin/travels', 'AdminTravelController@index');



	Route::get('/admin/users/{id}/edit', 'AdminUserController@edit');
	Route::get('/admin/users', 'AdminUserController@index');
	Route::get('/admin/users/search', 'AdminUserController@search');
	Route::get('/admin/users/create', 'AdminUserController@create');
	Route::get('/admin/users/{id}', 'AdminUserController@show');
	Route::post('/admin/users', 'AdminUserController@store');
	Route::put('/admin/users/{id}', 'AdminUserController@update');
	Route::get('/admin/users/{id}/delete', 'AdminUserController@destroy');
});






// Route::get('/admin/orders', function(){
// 	return view('admin.pages.orders');
// });
