@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">  
		<div class="col-md-8">
			<h1>Register</h1>
			@include('layouts.errors')
			<form method="POST" action="/register">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" class="form-control" value="" id="name" name="name">
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" value="" id="email" name="email">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" value="" id="password" name="password">
				</div>
				<div class="form-group">
					<label for="password_confirmation">Confirm Password</label>
					<input type="password" class="form-control" value="" id="password_confirmation" name="password_confirmation">
				</div>
				<div class="form-group">
					<input type="submit" value="Save" class="btn btn-primary">
				</div>
			</form>
		</div><!--/ col-md-8 -->
</div><!--/ row -->
</div><!--/ container -->   	
@endsection