@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h3>Confirm your delivery address</h3>

            <form method="POST" action="{{ action('OrderController@create') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="Telephone">Telephone</label> <input name="shipping_telephone" class="form-control" placeholder="Telephone" type="text" value="{{ auth()->user()->telephone }}">
                </div>

                <div class="form-group">
                    <label for="city">City</label> <input name="shipping_city" class="form-control" placeholder="City" type="text" value="{{ auth()->user()->city }}">
                </div>

                <div class="form-group">
                    <label for="state">State</label> <input name="shipping_state" class="form-control" placeholder="State" type="text" value="{{ auth()->user()->state }}">
                </div>

                <div class="form-group">
                    <label for="country">Country</label> 
                    {!! Form::select('shipping_country_id', \App\Country::get()->pluck('country_name', 'id'), auth()->user()->country_id, ['placeholder' => 'Select Country', 'class' => 'form-control select2', 'style' => 'width:100%;']) !!}


                </div>

                <div class="form-group">
                    <label for="postal_code">Postal Code</label> <input name="shipping_postal_code" class="form-control" placeholder="Postal Code" type="text" value="{{ auth()->user()->postal_code }}">
                </div>

                <div class="form-group">
                    <label for="address">Address</label> <input name="shipping_address" class="form-control" placeholder="Address" type="text" value="{{ auth()->user()->address }}">
                </div>

                <div class="checkbox">
                </div><button class="btn btn-primary" type="submit">Pay And Place Order</button>
            </form>

        </div>

        <div class="col-md-4">
        	@include('layouts.right-cart-cart')
        </div><!--/ col-md-4 -->
    </div><!--/ row -->
</div><!--/ container -->    
@endsection

@section('footer')
    @include('modals.confirm-shipping-address')
@endsection