@extends('layouts.layout')
@section('title') @stop
@section('css')
    <link href="/zoom/jquery.exzoom.css" rel="stylesheet" type="text/css"/> 
@stop
@section('js')
    {{-- https://www.jqueryscript.net/slider/Product-Carousel-Magnifying-Effect-exzoom.html --}}
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="/zoom//jquery.exzoom.js"></script>

    <script>
        $(function(){

          $("#exzoom").exzoom({

            // thumbnail nav options
            "navWidth": 60,
            "navHeight": 60,
            "navItemNum": 5,
            "navItemMargin": 7,
            "navBorder": 0,

            // autoplay
            "autoPlay": false,

            // autoplay interval in milliseconds
            "autoPlayTimeout": 2000
            
          });

        });
    </script>

@stop
@section('content')
<div class="container">
    <div class="row">
        @if(count($product))
            <!-- Blog Entries Column -->
            <div class="col-md-8">
            	<div class="product-gallery">
            		<h3>Gallery</h3>
                    {{-- {{ dd($product) }} --}}
            		<div class="product-gallery-images">

                        <div class="exzoom" id="exzoom" style="width:400px; height: 400px; margin: 0 auto;">
                            <!-- Images -->
                            <div class="exzoom_img_box">
                                <ul class='exzoom_img_ul'>
                                    @foreach($product['images'] as $key => $type)
                                        <li><img src="{{ $type['large_image'] }}"/></li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- <a href="https://www.jqueryscript.net/tags.php?/Thumbnail/">Thumbnail</a> Nav-->
                            <div class="exzoom_footer">
                                <div class="exzoom_nav"></div>
                                <!-- Nav Buttons -->
                                <p class="exzoom_btn">
                                  <a href="javascript:void(0);" class="exzoom_prev_btn"> < </a>
                                  <a href="javascript:void(0);" class="exzoom_next_btn"> > </a>
                                </p>
                            </div><!--/ exzoom_footer -->


                        </div>

            			{{-- <img src="{{ $product['LargeImage'] }}" alt=""> --}}
            		</div><!--/ product-gallery-images -->
            	</div><!--/ product-gallery -->
            	<div class="product-feature">
            		<h3>Feature</h3>
            		<ul>
            			<li>
            				{!! join('</li><li>', $product['feature'] ) !!}
            			</li>
            		</ul>
            	</div><!--/ product-feature -->

                 <div class="product-reviews" style="margin-top:100px;">
                    <h3>Reviews</h3>
                    {{-- {{ dd($product) }} --}}
                    <iframe style="width: 100%; height: 400px;" src="{{ $product['customer_reviews'] }}" frameborder="0"></iframe>
                </div><!--/ product-reviews -->

            	<!-- <div class="product-description">
                    <h3>Desciption</h3>
                </div>/ product-description -->
            </div>
            <div class="col-md-4">
            	<div class="product-details">

            		<div class="product-title">
            			<h3 class="product-title">{{ $product['title'] }}</h3>
            			<div class="original-link">By <a href="{{ $product['affiliates_url'] }}" target="_blank">Amazon.in</a></div>
            		</div><!--/ product-title -->

        			<div class="product-price">
                        <div class="price">
                                @if($product['total'] < $product['price'])
                                    <span class="without-offer-price">{{ on_bdt($product['price']) }}</span>
                                @endif
                            <span class="main-price">{{ on_bdt($product['total']) }}</span>
                        </div>
        			</div>

        			<div class="buy-now">
        				<a id="buy" class="btn btn-default add_to_cart" item_id="{{ $product['id'] }}" href="/add-to-cart?item_id={{ $product['id'] }}" amazon_id="{{ $product['id'] }}">Add to Cart</a>
        			</div>

        			{{-- <div class="estimation">Order now, get it within<strong> Nov 3 - 6</strong></div> --}}

                    @if($product['parent_id'])
                        <div class="product-variations">
                            <h4>Color &amp; Sizes</h4>
                            <form action="">
                                <select class="form-control" id="single-product-variations">
                                  {!! \App\Product::get_variations($product['parent_id'], $product['id']) !!}
                                  {{-- <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option> --}}
                                </select>
                            </form>
                        </div><!--/ variations -->
                    @endif
        			
        			{{-- <div class="payment-logos">
        				<img alt="Payment Methods" title="Payment Methods" src="{{ URL::asset('images/payment-icons.png') }}">
        			</div> --}}
            			
            		{{-- {{ dd($product) }} --}}
            	</div><!--/ product-details -->
            </div>
        @else
            <h2>No Data</h2>
        @endif    
    </div><!--/ row -->
</div><!--/ container -->   
@endsection