@extends('layouts.layout')
@section('content')
    <!-- Blog Entries Column -->
<div class="container">
    <div class="row">
        <div class="col-md-8">
            {{-- {{ dd($cart_session->cart['items'])}} --}}
       		@if(count($cart_items))
    	        <table class="table table-striped cart-table">
    				<thead>
    					<tr>
    						<th>Image</th>
    						<th>Title</th>
    						<th>Price</th>
    						<th>Qty</th>
    					</tr>
    				</thead>
    				<tbody>

    						@foreach($cart_items as $item)
    							<tr>
    								<th class="cart-r-img" scope="row"><img src="{{ $item['product_image'] }}" alt=""></th>
    								<td class="cart-r-title">
                                        <a  href="{{ URL("/products") }}/{{ $item['id'] }}">{{ $item['title'] }}</a>
                                        <span class="cart-r-tit-foot">
                                            <a class="plus_item" href="/plus_item?item_id={{ $item['id'] }}"><i class="tit-d-in fa fa-heart-o" aria-hidden="true"></i></a>

                                            <a class="remove_item"
                                                 item_id="{{ $item['id'] }}" 
                                                 href="/remove_item?item_id={{ $item['id'] }}">
                                                 <i class="fa fa-trash tit-d-in" aria-hidden="true"></i>
                                             </a>
                                        </span>
                                    </td>
    								<td class="cart-r-price">{{ on_bdt($item['total']) }}</td>
    								<td class="cart-r-count">

                                        <a class="plus_item" 
                                            item_id="{{ $item['id'] }}" 
                                            href="/plus_item?item_id={{ $item['id'] }}">
                                            <i class="tit-d-cnt fa fa-plus-circle" aria-hidden="true"></i>
                                        </a>

                                        <input class="tit-d-cnt" type="text" value="{{ $item['qty'] }}">

                                        <a class="minus_item"
                                             item_id="{{ $item['id'] }}" 
                                             href="/minus_item?item_id={{ $item['id'] }}">
                                             <i class="tit-d-cnt fa fa-minus-circle" aria-hidden="true"></i>
                                         </a>

                                    </td>
    							</tr>
    						@endforeach
    					
    				</tbody>
    			</table>
    			<div class="foot-cart-list">
    				<a href="{{ action('ProductController@resetCart') }}" class="btn btn-default">Clear Cart</a>
    			</div><!--/ foot-cart-list -->
            @else
                <h4>Your cart is empty</h4>
            @endif
        </div>

        <div class="col-md-4">
        	@include('layouts.right-cart-cart')

            <div class="order-checkout-cart">
                @if(auth()->check())
                    <a class="btn btn-default" href="{{ url('products/checkout') }}">Proceed To Checkout</a>
                @else
                    <a class="btn btn-default" href="#" data-toggle="modal" data-target="#signin">Proceed To Checkout</a>
                @endif
            </div><!--/ order-checkout-cart -->
            
        </div><!--/ col-md-4 -->
    </div><!--/ row -->
</div><!--/ container -->    
@endsection