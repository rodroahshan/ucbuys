<li class="product-small col-lg-3 col-md-4 col-sm-6 col-xs-6">
    <div class="main-container">
        <div class="main">
            <a href="/products/{{ $attr['id'] }}" title="">
                <img class="scale" data-scale="best-fit" data-align="center" src="{{ $attr['product_image'] }}" alt="">
            </a>                
        </div>
    </div>
    <div class="price">
            {{-- <p>{{ $attr['offer_price'] }}</p> --}}
            @if($attr['total'] < $attr['price'])
                <span class="without-offer-price">{{ on_bdt($attr['price']) }}</span>
            @endif
        <span class="main-price">{{ on_bdt($attr['total']) }}</span>
    </div>
    <div class="foot">
        <div>
            <a href="/products/{{ $attr['id'] }}" title="">{{ $attr['title'] }}</a>
                <span class="product-small-rating">
                    <span class="flaticon-favorite-filled icon-star"></span>
                    <span class="flaticon-favorite-filled icon-star"></span>
                    <span class="flaticon-favorite-filled icon-star"></span>
                    <span class="flaticon-favorite-filled icon-star"></span>
                    <span class="flaticon-favorite-filled icon-star disabled"></span>
                </span>
            <div class="discount">
                <!-- react-text: 322 --><!-- /react-text -->
            </div>
        </div>
        <div class="add-cart-index">
            <a class="btn btn-default add_to_cart" item_id="{{ $attr['id'] }}" href="/add-to-cart?item_id={{ $attr['id'] }}">Add To Cart</a>
        </div><!--/ add-cart-index -->
    </div>
</li>