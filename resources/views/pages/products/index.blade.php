@extends('layouts.layout')
@section('content')

@if(!request('page'))
    <div class="container">
        <div class="banner-section">
            {{-- <div class="banner-text">
                <h1>want anything from amazon.in or aliexpress.com?</h1>
                <h2>bdboom.com</h2> --}}
                {{-- <h1>আপনি কি কিছু কিনতে চান ইন্ডিয়া অথবা usa থেকে?</h1>
                <h2>বিডিবুম.কম</h2> --}}
            {{-- </div> --}}<!--/ banner-text -->
            <img src="{{ url('/images/banner/banner-2.jpg')}}" alt="">
        </div><!--/ banner -->
    </div><!--/ container -->
@endif

<div class="container">
    <div class="category-menu">

        {{-- <ul>
            @foreach(\App\Amazon::categories() as $category => $param)
                <li><a href="/?category={{$category}}">{{$category}}</a></li>
            @endforeach
        </ul> --}}
            
            @foreach(\App\AmazonCategory::$fasion as $cat => $cat_list)
                <ul class="maga-menu-item">
                    <li><a href="{{ url('/products/search/?query')}}={{$cat_list['name']}}">{{ $cat_list['name' ]}}</a>
                        <ul>
                            @foreach($cat_list['items'] as $item)
                                <li><a href="{{ url('/products/search/?query')}}={{$item}}">{{$item}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            @endforeach
    </div><!--/ category-menu -->
</div><!--/ container -->

@if(request('page'))
    <div class="container">
        <div class="row">
            {!! $pagination !!}
        </div><!--/ row -->
    </div><!--/ container -->
@endif


{{-- <div class="container">
    <h1>Buy From India Amazon Get It From Bd</h1>
</div> --}}
    <!-- Blog Entries Column -->
<div class="container">
    <div class="row">  
            <ul class="items">
                {{-- {{ dd($products) }} --}}
                @if(count($products))
                    @foreach($products as $product => $attr)
                        @include('pages.products.product')
                    @endforeach
                @else
                    <h3>No Item Found</h3>
                @endif                
            </ul>
        </div>
        	{!! $pagination !!}
        </div>
    </div><!--/ row -->
</div><!--/ container -->       
@endsection