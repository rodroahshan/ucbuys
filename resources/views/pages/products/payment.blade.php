@extends('layouts.layout')
@section('content')
    <!-- Blog Entries Column -->
    <?php $cart_session = new \App\Cart; ?>
<div class="container">
    <div class="row">    
	    <div class="col-md-8">
	        <h2>Choose Payment</h2>
	   	</div>

	   	<div class="col-md-4">
	    	<div class="right-cart">
	    		<h2>Summery</h2>
	    		<div class="order-sum-cart">
	    			<ul>
	    				<li>
		    				<span class="sum-label">SubTotal {{ $cart_session->net_qty }} Items</span>
		    				<span class="sum-value">{{ \App\Amazon::amount_format($cart_session->sub_total) }}</span>
	    				</li>
	    				<li>
		    				<span class="sum-label">Shipping Fee</span>
		    				<span class="sum-value">{{ \App\Amazon::amount_format($cart_session->shipping) }}</span>
	    				</li>
	    			</ul>
	    		</div><!--/ order-sum-cart -->
	    		<div class="order-total-cart">
	    			<div class="order-total-cart-label">Total</div>
	    			<div class="order-total-cart-value">{{ \App\Amazon::amount_format($cart_session->grand_total) }}</div>
	    		</div><!--/ order-total-cart -->
	    	</div><!--/ right-cart -->
	    </div><!--/ col-md-4 -->	
	</div><!--/ row -->
</div><!--/ container -->       
    
@endsection