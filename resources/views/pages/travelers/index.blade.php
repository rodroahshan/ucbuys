@extends('layouts.layout')
@section('content')
    <section id="travel-banner">
        <div class="container">
            <div class="row">  
                <div class="col-md-7">
                    <div class="traveler-banner-title">
                        <h2 class="b-tit">Earn Every Time You Travel With Us</h2>
                        <p class="b-sub-tit">Carry products for others in your extra luggage space to make money while traveling.</h2>
                    </div><!--/ traveler-banner-quote -->
                </div><!--/ col-md-7 -->
                <div class="col-md-5">
                    <div class="traveler-signup-form">
                        <h2>Get Started</h2>
                        <form method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="travel_to">Travel To</label> 
                                {{  Form::select('travel_to', [1 => "Bangladesh"], null, ['placeholer' => "Travel To", 'class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                <label for="travel_to">How much luggage space do you have?</label> 
                                {{  Form::select('travel_to', [1 => "One Luckage", 2 => "Two Luckage", 3 => "Three Luckage"], null, ['placeholer' => "Travel To", 'class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                <div class="trav-est-earn">
                                    <label>Estamate Earning</label> 
                                    <p>৳12000</p>
                                </div><!--/ trav-est-earn -->
                            </div>

                            <button class="btn btn-default" type="submit">Proceed</button>
                        </form>
                    </div><!--/ traveler-signup-form -->
                </div>
            </div><!--/ row -->
        </div><!--/ container -->
        {{-- <img src="{{ url('images/traveler/traveler-banner-1.jpg')}}" alt=""> --}}
    </section>
@endsection