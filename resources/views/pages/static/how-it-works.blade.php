@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>How It Works</h3>

            <div class="how-it-works-sec">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cw_about_box_hover">
                    <div class="how-it-works-box">
                        <ul>
                            <li><p> Search your product from bdboom.com or paste link in bdboom.com search bar from amazon.in product url.</p></li>
                            <li><p> Select product qty and color and size.</p></li>
                            <li><p> Select your delivery info (Name, Address, Phone)</p></li>
                            <li><p> You have to pay 100% advance when you will order the product.</p></li>
                            <li><p> You will get a notice after we got the products. </p><p></p></li>
                            <li><p> When we will receive your product, we will inform you.</p></li>
                            <li><p> We will deliver you product within 2 days after we recieved.</p></li>
                            <li><p> When you will receive your product, inform us and leave your feedback in our website. </p></li>
                        </ul>
                    </div>
                </div>

	            <h3> Accepted Payments Type </h3>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cw_about_box_hover">
                    <div class="how-it-works-box">
                        <ul>
                            <li><p>BKASH</p></li>
                            <li><p>ROCKET</p></li>
                            <li><p>VISA</p></li>
                            <li><p>MASTER CARD</p></li>
                        </ul>
                    </div>
                </div>

            </div>
            
        </div>
    </div><!--/ row -->
</div><!--/ container -->    
@endsection
