@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Faq</h3>

            <div class="row gap-y">

            	<div class="col-md-6">
		            <h5>When my product will be delivered?</h5>
		            <p>Bdboom will take 21-35 days to arrive product from India. After receiving your products, we will send your items as soon as possible.</p>
		        </div>


		        <div class="col-md-6">
		            <h5>What payment services do you support?</h5>
		            <p>Currently, We are accepting visa, dbbl, mastercard, bKash, Rocket, ucash</p>
		        </div>


		        


		        <div class="col-md-6">
		            <h5>Is their any Facebook page?</h5>
		            <p>Yes, The
		                <a href="https://www.facebook.com/Bd-Boom-361982474397468" class="badge">BDBOOM</a>You can order your product directly here or in the Facebook Group. If you order your products by bdboom directly, then our customer agent will be assigned to assist you.
		            </p>
		        </div>


		        <div class="col-md-6">
		            <h5>Can I track my products?</h5>
		            <p>Yes. After successful purchase, we will go to dahsboard and get the order details. You can find tracking status in your order details page on the account panel.</p>
		        </div>


		        <div class="col-md-6">
		            <h5>Can I request a refund?</h5>
		            <p>Yes, We will refund for every unsuccessful purchase or broken product received. Please read your
		                <a href="#refund-policy">Refund Policy</a> to know more about refund.</p>
		        </div>

		    </div>
            
        </div>
    </div><!--/ row -->
</div><!--/ container -->    
@endsection

</div>