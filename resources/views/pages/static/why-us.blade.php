@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Why Us</h3>

            <div class="row">

                <div class="col-md-12">
                    <h5>Reliable</h5>
                    <p>Your products is reliable and whole responsibilities is ours.</p>
                </div>

        
                <div class="col-md-12">
                    <h5>Support Assistant</h5>
                    <p>Our admin always take care of your product till the arrival at your destination.</p>
                </div>


        
                <div class="col-md-12">
                    <h5>Tracking</h5>
                    <p>You will get every order tracking status. You can track where is your item.</p>
                </div>


        
                <div class="col-md-12">
                    <h5>Easy to use</h5>
                    <p>You will get a dashboard to see your orders, wishlist etc. You will see your order details, invoices detailed there</p>
                </div>

        
                <div class="col-md-12">
                    <h5>Refund</h5>
                    <p>We will refund your money if a product isn't purchase yet or broken product received by you.</p>
                </div>
            </div>
            
        </div>
    </div><!--/ row -->
</div><!--/ container -->    
@endsection

</div>