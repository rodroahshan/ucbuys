@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Contact Us</h3>

            <p>Feel free to talk to our online representative at any time you please using our Live Chat system on our website or one of the below instant messaging programs.</p>

			<p>Please be patient while waiting for response. (24/7 Support!) Phone General Inquiries: +88 0191 44 88 363</p>

			<h3>Company Address</h3>
			<h4>bdboom.com</h4>
			<p>
				56, East Tejturi Bazar, Rahman Mansion (2nd Floor), Near ​Farmgate Over, Bridge, Dhaka 1215<br />
				<b>Mobile:</b> +88 0191 44 88 363, 01914 22 12 43, 016 80 81 63 43,<br />
				<b>E-mail: </b>support@bdboom.com<br />
				<b>Website: www.bdboom.com</b>
			</p>
            
        </div>
    </div><!--/ row -->
</div><!--/ container -->    
@endsection
