<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Lumino - Dashboard</title>

		<?php $settings = \App\Http\Controllers\HelperController::$admin_settings; ?>
	    @foreach($settings['css'] as $css)
	        <link rel="stylesheet" href="{{ $css }}">
	    @endforeach

		<!--Theme Switcher-->
		<style id="hide-theme">
			body{
				display:none;
			}
		</style>
		<script type="text/javascript">
			function setTheme(name){
				var theme = document.getElementById('theme-css');
				var style = 'css/theme-' + name + '.css';
				if(theme){
					theme.setAttribute('href', style);
				} else {
					var head = document.getElementsByTagName('head')[0];
					theme = document.createElement("link");
					theme.setAttribute('rel', 'stylesheet');
					theme.setAttribute('href', style);
					theme.setAttribute('id', 'theme-css');
					head.appendChild(theme);
				}
				window.localStorage.setItem('lumino-theme', name);
			}
			var selectedTheme = window.localStorage.getItem('lumino-theme');
			if(selectedTheme) {
				setTheme(selectedTheme);
			}
			window.setTimeout(function(){
					var el = document.getElementById('hide-theme');
					el.parentNode.removeChild(el);
				}, 5);
		</script>
		<!-- End Theme Switcher -->


		<!--Custom Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="{{ url('backend/js/html5shiv.js') }}"></script>
		<script src="{{ url('backend/js/respond.min.js') }}"></script>
		<![endif]-->

		
	</head>
	<body>
		@include('admin.partials.topbar')
		@include('admin.partials.sidebar')
		
			
		<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">

			@include('admin.partials.breadcrump')
			@include('admin.partials.errors')
			
			@yield('main')

		</div>	<!--/.main-->
		
		<script type="text/javascript">
        window.base_url = <?php echo json_encode(url('/')); ?>;
	    </script>

	    <?php $settings = \App\Http\Controllers\HelperController::$admin_settings; ?>
	    @foreach($settings['js'] as $js)
	        <script src="{{ $js }}"></script>
	    @endforeach

		<script>
			window.onload = function () {
				var chart1 = document.getElementById("line-chart").getContext("2d");
				window.myLine = new Chart(chart1).Line(lineChartData, {
				responsive: true,
				scaleLineColor: "rgba(0,0,0,.2)",
				scaleGridLineColor: "rgba(0,0,0,.05)",
				scaleFontColor: "#c5c7cc"
				});
			};
		</script>
			
	</body>
</html>