@extends('admin.layouts.layout')
@section('title')Travels @stop
@section('main')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">All Travels</div>
				<div class="panel-body btn-margins">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Traveler Name</th>
									<th>From</th>
									<th>To</th>
									<th>Luckege Qty</th>
									<th>Travel Date</th>
								</tr>
							</thead>
							<tbody>
								@foreach($travels as $travel)
								<tr>
									<td>{{ $travel->id }}</td>
									<td>{{ $travel->user->name }}</td>
									<td>{{ $travel->cntry_fr->country_name }}</td>
									<td>{{ $travel->cntry_to->country_name }}</td>
									<td>{{ $travel->luckege_qty }}</td>
									<td>{{ formated_date($travel->travel_date) }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $travels->links() }}
					</div>
				</div>
			</div><!-- /.panel-->
		</div><!--/ col-md-12 -->
	</div><!--/ row -->
@endsection	