@extends('admin.layouts.layout')
@section('title') Orders @stop
@section('main')

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">All Orders</div>
				<div class="panel-body btn-margins">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>User</th>
									<th>Product Qty</th>
									<th>Product Cost</th>
									<th>Shipping Cost</th>
									<th>Order Total</th>
									<th>Payment Status</th>
								</tr>
							</thead>
							<tbody>
								@foreach($orders as $order)
								<tr>
									<td>{{ $order->id }}</td>
									<td><a href="{{ url('admin/orders', $order->id) }}">{{ $order->user->name }}</a></td>
									<td>{{ $order->no_of_products }}</td>
									<td>{{ $order->product_cost }}</td>
									<td>{{ $order->shipping_cost }}</td>
									<td>{{ $order->order_total }}</td>
									<td>{{ $order->payment_id ? 'Paid' : 'Unpaid' }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div><!-- /.panel-->
		</div><!--/ col-md-12 -->
	</div><!--/ row -->
@endsection	