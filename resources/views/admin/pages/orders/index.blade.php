@extends('admin.layouts.layout')
@section('title') Orders @stop
@section('main')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="search-big-admin">
						{!! Form::open(['url' => action('AdminOrderController@search'), 'method' => 'get']) !!}

							<div class="col-md-3 input-group-sm">
								<label for="">Keyword</label>
								{!! Form::text($field="keyword", $value='', ['placeholder' => '#id', 'class' => 'form-control']); !!}
							</div><!--/ col-md-3 -->

							<div class="col-md-3">
								<label for="">Delivery Status</label>
								{!! Form::select($field="delivery_status", $data=\App\Order::$delivery_status, $value='', ['placeholder' => 'Select Delivery Status', 'class' => 'form-control']); !!}
							</div><!--/ col-md-3 -->

							<div class="col-md-3">
								<label for="">Payment Status</label>
								{!! Form::select($field="payment_status", $data=\App\Order::$payment_status, $value='', ['placeholder' => 'Select Payment Status', 'class' => 'form-control']); !!}
							</div><!--/ col-md-3 -->

							<div class="col-md-3">
								<div class="form-group">
									{!! Form::submit('Search', ['class' => ' btn btn-primary']) !!}
								</div><!--/ form-group -->
							</div><!--/ col-md-3 -->
						{!! Form::close() !!}
					</div><!--/ big-search-admin -->
				</div><!--/ row -->
				<div class="clearfix"></div><!--/ clearfix -->
				{{-- <div class="row">
					<div class="col-md-4">
						<a href="{{ url('/admin/orders/create') }}" class="btn btn-primary">+Add New</a>
					</div>
					<div class="col-md-3 col-md-offset-5">
						<form action="{{ action('AdminOrderController@search') }}">
							<div class="form-group">
								<input type="text" name="keyword" class="form-control" placeholder="Search" value="{{ request('keyword') ? request('keyword') : '' }}">
							</div>
						</form>
					</div>
				</div> --}}
			</div>
			<div class="panel-body btn-margins">
				<div class="col-md-12">
					<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>User</th>
									<th>Product Qty</th>
									{{-- 
									<th>Product Price</th>
									<th>Shipping Price</th>
									<th>Bdboom Charge</th><th>Grand Total</th>
									<th>Discount</th>
									<th>Delivery Charge</th><th>All Total</th>
									<th>Gateway Charge</th>
									 --}}
									<th>Master Total</th>
									<th>Paid Amount</th>
									<th>Payment Status</th>
									<th>Delivery Status</th>
									<th>Date</th>
									<th>Items</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($orders as $order)
								<tr>
									<td>{{ $order->id }}</td>
									<td>@if($order->user)<a href="{{ action('AdminUserController@show', $order->user->id) }}">{{ $order->user->name }}</a>@endif</td>
									<td>{{ $order->grand_qty }}</td>
									{{-- 
									<td>{{ on_bdt($order->grand_net_price) }}</td>
									<td>{{ on_bdt($order->grand_shipping_charge) }}</td>
									<td>{{ on_bdt($order->grand_bdboom_charge) }}</td>
									<td>{{ on_bdt($order->grand_total) }}</td>
									<td>{{ on_bdt($order->discount) }}</td>
									<td>{{ on_bdt($order->delivery_charge) }}</td>
									<td>{{ on_bdt($order->all_total) }}</td>
									<td>{{ on_bdt($order->gateway_charge) }}</td>
									 --}}
									<td>{{ on_bdt($order->master_total) }}</td>
									<td>@if($order->payment) {{ on_bdt($order->payment->trans_amount) }} @endif</td>
									<td>{{ \App\Order::get_payment_status_name_by_key($order->payment_status) }}</td>
									<td>{{ \App\Order::get_delivery_status_name_by_key($order->delivery_status) }}</td>
									<td>{{ $order->created_at->toFormattedDateString() }}</td>
									<td><a href="{{ action('AdminOrderController@products', $order->id) }}">See Items</a></td>
									<td>
										<a class="btn btn-primary" href="{{ action('AdminOrderController@show', $order->id) }}">View</a>
										<a class="btn btn-primary" href="{{ action('AdminOrderController@edit', $order->id) }}">Edit</a>
										<a class="btn btn-danger del" href="{{ action('AdminOrderController@destroy', $order->id) }}">Delete</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					{{ $orders->appends(['keyword' => request('keyword')])->links() }}
				</div>
			</div>
		</div><!-- /.panel-->
	</div><!--/ col-md-12 -->
</div><!--/ row -->
@endsection	