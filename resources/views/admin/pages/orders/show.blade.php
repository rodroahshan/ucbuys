@extends('admin.layouts.layout')
@section('title') Products @stop
@section('main')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="admin-order-show-top-menu">

							<div class="col-md-3">
								<div class="aostm-box">
									ORDER #{{$order->id}}
								</div><!--/ aostm-box -->
							</div><!--/ col-md-3 -->

							<div class="col-md-3">
								<div class="aostm-box">
									{{$order->user->name}}
								</div><!--/ aostm-box -->
							</div><!--/ col-md-3 -->
									
							<div class="col-md-3">
								<div class="aostm-box">
									{{ \App\Order::get_delivery_status_name_by_key($order->delivery_status) }}
								</div><!--/ aostm-box -->
							</div><!--/ col-md-3 -->

							<div class="col-md-3">
								<div class="aostm-box">
									<a href="{{ action('AdminOrderController@edit', $order->id )}}" class="btn btn-primary">Edit</a> 
								</div><!--/ aostm-box -->
							</div><!--/ col-md-3 -->


						</div><!--/ admin-order-show-top-menu -->
					</div><!--/ row -->

				</div>
				<div class="panel-body btn-margins">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th>Id</th>
									<th>Image</th>
									<th>Title</th>
									<th>Product Url</th>
									<th>Product Source</th>
									{{-- <th>Image</th> --}}
									<th>Quantity</th>
									<th class="text-right">Price</th>
								</tr>
							</thead>
							<tbody>
								@foreach($order->products as $product)
								<tr>
									<td>#{{ $product->id }}</td>
									<td><img width="50" src="{{ $product->product_image }}"></td>
									<td><a target="_blank" href="{{ action('ProductController@show', $product->product_id) }}">{{ str_limit($product->title, 50) }}</a></td>
									<td><a target="_blank" href="{{ $product->affiliates_url }}">{{ str_limit($product->affiliates_url, 50) }}</a></td>
									<td>{{ \App\OrderProduct::get_product_src_by_key($product->product_src, $lookup='name') }}</td>
									{{-- <td><img class="sm-thumb" src="{{ $product->product_image }}" alt=""></td> --}}
									<td>{{ $product->qty }}</td>
									<td class="text-right">{{ on_bdt($product->price) }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="6"><span>Product Total</span></td>
									<td class="text-right"><b>{{ on_bdt($order->grand_total) }}</b></td>
								</tr>
								<tr>
									<td class="text-right" colspan="6">Delivery Fee</td>
									<td class="text-right"><b>{{ on_bdt($order->delivery_charge) }}</b></td>
								</tr>
								<tr>
									<td class="text-right" colspan="6">Discount(-)</td>
									<td class="text-right"><b>{{ on_bdt($order->discount) }}</b></td>
								</tr>
								<tr>
									<td class="text-right" colspan="6">Gateway Charge</td>
									<td class="text-right"><b>{{ on_bdt($order->gateway_charge) }}</b></td>
								</tr>
								{{-- <tr>
									<td class="text-right" colspan="6"><span>Free Shipping</span></td>
									<td class="text-right"><b>tk.0</b></td>
								</tr> --}}
								<tr class="total-big">
									<td class="text-right" colspan="6"><span>Payable</span></td>
									<td class="text-right"><b>{{ on_bdt($order->master_total) }}</b></td>
								</tr>
								<tr class="total-big">
									<td class="text-right" colspan="6"><span>Paid</span></td>
									<td class="text-right"><b>@if($order->payment){{ on_bdt($order->payment->trans_amount) }}@endif</b></td>
								</tr>
							</tfoot>
						</table>
						{{-- {{ $products->links() }} --}}
					</div>
				</div>
			</div><!-- /.panel-->
		</div><!--/ col-md-12 -->
	</div><!--/ row -->
@endsection	