@extends('admin.layouts.layout')
@section('title') Products @stop
@section('main')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">All Products</div>
				<div class="panel-body btn-margins">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Title</th>
									<th>Product Url</th>
									<th>Image</th>
									<th>Quantity</th>
									<th>Price</th>
									<th>Order By</th>
								</tr>
							</thead>
							<tbody>
								@foreach($products as $product)
								<tr>
									<td>{{ $product->id }}</td>
									<td>{{ $product->title }}</td>
									<td><a target="_blank" href="{{ $product->product_url }}">{{ str_limit($product->product_url, 50) }}</a></td>
									<td><img class="sm-thumb" src="{{ $product->product_image }}" alt=""></td>
									<td>{{ $product->quantity }}</td>
									<td>{{ on_bdt($product->price) }}</td>
									<td>{{ $product->order->user->name }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $products->links() }}
					</div>
				</div>
			</div><!-- /.panel-->
		</div><!--/ col-md-12 -->
	</div><!--/ row -->
@endsection	