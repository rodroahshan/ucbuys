@extends('admin.layouts.layout')
@section('title') Users @stop
@section('main')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-4">
							<a href="{{ url('/admin/users/create') }}" class="btn btn-primary">+Add New</a>
						</div>
						<div class="col-md-3 col-md-offset-5">
							<form action="{{ action('AdminUserController@search') }}">
								<div class="form-group">
									<input type="text" name="keyword" class="form-control" placeholder="Search" value="{{ request('keyword') ? request('keyword') : '' }}">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="panel-body btn-margins">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Email</th>
									<th>Role</th>
									<th>Date</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->m_role->name }}</td>
									<td>{{ $user->created_at->toFormattedDateString() }}</td>
									<td>
										<a class="btn btn-primary" href="{{ action('AdminUserController@show', $user->id) }}">View</a>
										<a class="btn btn-primary" href="{{ action('AdminUserController@edit', $user->id) }}">Edit</a>
										<a class="btn btn-danger del" href="{{ action('AdminUserController@destroy', $user->id) }}">Delete</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $users->appends(['keyword' => request('keyword')])->links() }}
					</div>
				</div>
			</div><!-- /.panel-->
		</div><!--/ col-md-12 -->
	</div><!--/ row -->
@endsection	