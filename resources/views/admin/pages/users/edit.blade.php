@extends('admin.layouts.layout')
@section('title') Users @stop
@section('main')
	{{-- {{ dd($user) }} --}}
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">Edit User</div>
				<div class="panel-body">
					{!! Form::open(['url' => '/admin/users/'. $user->id, 'method' => 'put', 'class' => 'form-horizontal row-border']) !!}

						{!! myText($title='Your Name', $field='name', $value="$user->name", ['placeholder' => 'Enter Your Name', 'class' => 'form-control']) !!}
						
						{!! mySelect($title='Role', $field='role', $data=\App\Role::get()->pluck('name', 'key'), $value=$user->role, ['placeholder' => 'Select Role', 'class' => 'form-control select2', 'style' => 'width:100%;']) !!}

						{!! myEmail($title='Email', $field='email', $value="$user->email", ['placeholder' => 'Enter Your Email', 'class' => 'form-control']) !!}

						{!! myText($title='Telephone', $field='telephone', $value="$user->telephone", ['placeholder' => 'Enter Your Telephone', 'class' => 'form-control']) !!}

						{!! myTextArea($title='Address', $field='address', $value="$user->address", ['placeholder' => 'Enter Your Address', 'class' => 'form-control']) !!}

						{!! myText($title='City', $field='city', $value="$user->city", ['placeholder' => 'Enter Your City', 'class' => 'form-control']) !!}

						{!! myText($title='State', $field='state', $value="$user->state", ['placeholder' => 'Enter Your State', 'class' => 'form-control']) !!}
						{!! myText($title='Postal Code', $field='postal_code', $value="$user->postal_code", ['placeholder' => 'Enter Your Postal Code', 'class' => 'form-control']) !!}

						{!! mySelect($title='Country', $field='country_id', $data=\App\Country::get()->pluck('country_name', 'id'), $value=$user->country_id, ['placeholder' => 'Select Country', 'class' => 'form-control select2', 'style' => 'width:100%;']) !!}

						{!! mySubmit($title='Save', ['class' => 'btn btn-lg btn-primary']) !!}

					{!! Form::close() !!}
						
					</form>
				</div>
			</div>
		</div>
	</div><!--/.row-->
@endsection	