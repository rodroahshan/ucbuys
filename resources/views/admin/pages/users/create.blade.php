@extends('admin.layouts.layout')
@section('title') Users @stop
@section('main')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">Add User</div>
				<div class="panel-body">
					{!! Form::open(['url' => '/admin/users', 'method' => 'post', 'class' => 'form-horizontal row-border']) !!}

						{!! myText($title='Your Name', $field='name', $value='', ['placeholder' => 'Enter Your Name', 'class' => 'form-control']) !!}
						
						{!! mySelect($title='Role', $field='role', $data=\App\Role::get()->pluck('name', 'key'), $value=null, ['placeholder' => 'Select Role', 'class' => 'form-control select2', 'style' => 'width:100%;']) !!}

						{!! myEmail($title='Email', $field='email', $value='', ['placeholder' => 'Enter Your Email', 'class' => 'form-control']) !!}

						{!! myPassword($title='Password', $field='password', ['placeholder' => 'Enter Your Password', 'class' => 'form-control']) !!}
						{!! myPassword($title='Confirm Password', $field='password_confirmation', ['placeholder' => 'Enter Your Password', 'class' => 'form-control']) !!}

						{!! myText($title='Telephone', $field='telephone', $value='', ['placeholder' => 'Enter Your Telephone', 'class' => 'form-control']) !!}
						{!! myTextArea($title='Address', $field='address', $value='', ['placeholder' => 'Enter Your Address', 'class' => 'form-control']) !!}

						{!! myText($title='City', $field='city', $value='', ['placeholder' => 'Enter Your City', 'class' => 'form-control']) !!}
						{!! myText($title='State', $field='state', $value='', ['placeholder' => 'Enter Your State', 'class' => 'form-control']) !!}
						{!! myText($title='Postal Code', $field='postal_code', $value='', ['placeholder' => 'Enter Your Postal Code', 'class' => 'form-control']) !!}

						{!! mySelect($title='Country', $field='country_id', $data=\App\Country::get()->pluck('country_name', 'id'), $value=null, ['placeholder' => 'Select Country', 'class' => 'form-control select2', 'style' => 'width:100%;']) !!}

						{!! mySubmit($title='Save', ['class' => 'btn btn-lg btn-primary']) !!}

					{!! Form::close() !!}
						
					</form>
				</div>
			</div>
		</div>
	</div><!--/.row-->
@endsection	