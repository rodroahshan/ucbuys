@if(count($errors))
	<div class="admin-errors">
	     <ul class="alert alert-danger">
	         @foreach($errors->all() as $error)
	            <li>{{$error}}</li>
	          @endforeach
	     </ul>
	</div><!--/ admin-errors -->
@endif

@if(session()->has('message'))
	<div class="admin-errors">
	     <div class="alert alert-info">
            <p>{{session()->get('message')}}</p>
	     </div>
	</div><!--/ admin-errors -->
@endif