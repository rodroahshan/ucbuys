<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
		<em class="fa fa-envelope"></em><span class="label label-info">15</span>
	</a>
		<ul class="dropdown-menu dropdown-messages">
			<li>
				<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
					<img alt="image" class="img-circle" src="images/profile-pic-2.jpg" width="40">
					</a>
					<div class="message-body"><small class="pull-right">3 mins ago</small>
						<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
					<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
				</div>
			</li>
			<li class="divider"></li>
			<li>
				<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
					<img alt="image" class="img-circle" src="images/profile-pic-1.jpg" width="40">
					</a>
					<div class="message-body"><small class="pull-right">1 hour ago</small>
						<a href="#">New message from <strong>Jane Doe</strong>.</a>
					<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
				</div>
			</li>
			<li class="divider"></li>
			<li>
				<div class="all-button"><a href="#">
					<em class="fa fa-inbox"></em> <strong>All Messages</strong>
				</a></div>
			</li>
		</ul>
	</li>
	<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
		<em class="fa fa-bell"></em><span class="label label-primary">5</span>
	</a>
		<ul class="dropdown-menu dropdown-alerts">
			<li><a href="#">
				<div><em class="fa fa-envelope"></em> 1 New Message
					<span class="pull-right text-muted small">3 mins ago</span></div>
			</a></li>
			<li class="divider"></li>
			<li><a href="#">
				<div><em class="fa fa-heart"></em> 12 New Likes
					<span class="pull-right text-muted small">4 mins ago</span></div>
			</a></li>
			<li class="divider"></li>
			<li><a href="#">
				<div><em class="fa fa-user"></em> 5 New Followers
					<span class="pull-right text-muted small">4 mins ago</span></div>
			</a></li>
		</ul>
	</li>
	</ul>
	<ul class="navbar-right theme-switcher">
	<li><span>Choose Theme:</span></li>
	<li><a href="#" title="Default" data-theme="default" class="theme-btn theme-btn-default">Default</a></li>
	<li><a href="#" title="Iris" data-theme="iris" class="theme-btn theme-btn-iris">Iris</a></li>
	<li><a href="#" title="Midnight" data-theme="midnight" class="theme-btn theme-btn-midnight">Midnight</a></li>
	<li><a href="#" title="Lime" data-theme="lime"  class="theme-btn theme-btn-lime">Lime</a></li>
	<li><a href="#" title="Rose" data-theme="rose" class="theme-btn theme-btn-rose">Rose</a></li>