<div id="update-password-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Update Password</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ URL('/user/update_password') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="old_password">Old Password</label><input name="old_password" class="form-control" placeholder="Old Password" type="password">
            </div>
            <div class="form-group">
                <label for="password">New Password</label><input name="password" class="form-control" placeholder="New Password" type="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">Confirm Password</label> <input name="password_confirmation" class="form-control" placeholder="Confirm Password" type="password">
            </div>
            <div class="checkbox">
            </div><button class="btn btn-primary" type="submit">Update</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->