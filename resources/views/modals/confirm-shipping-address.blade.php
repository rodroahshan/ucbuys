<div id="confirm-shipping-address" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirm Your Devliery Address</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ action('OrderController@create') }}">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="shipping_city">City</label> <input name="shipping_city" class="form-control" placeholder="City" type="text" value="{{ auth()->user()->city }}" required>
            </div>

            <div class="form-group">
                <label for="shipping_state">State</label> <input name="shipping_state" class="form-control" placeholder="State" type="text" value="{{ auth()->user()->state }}" required>
            </div>


            <div class="form-group">
                <label for="shipping_address">Address</label> <input name="shipping_address" class="form-control" placeholder="Address" type="text" value="{{ auth()->user()->address }}" required>
            </div>

            <div class="form-group">
                <label for="shipping_telephone">Phone</label> <input name="shipping_telephone" class="form-control" placeholder="Phone" type="text" value="{{ auth()->user()->telephone }}" required>
            </div>


            <div class="checkbox">
            </div><button class="btn btn-primary" type="submit">Pay And Place Order</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->