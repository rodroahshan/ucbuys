<div id="signin" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Sign In</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ URL('/auth/login') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="email">Email address</label> <input name="email" class="form-control" id="email" placeholder="Email" type="email">
            </div>
            <div class="form-group">
                <label for="password">Password</label> <input name="password" class="form-control" id="password" placeholder="Password" type="password">
            </div>
            <div class="checkbox">
            </div><button class="btn btn-primary" type="submit">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{-- <a href="/auth/signup"><button type="button" class="btn btn-primary">Sign Up</button></a> --}}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->