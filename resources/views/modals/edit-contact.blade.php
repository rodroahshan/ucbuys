<div id="contact-edit-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Contact Information</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ URL('/user/update_user') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="email">Name</label> <input name="name" class="form-control" placeholder="Full Name" type="name" value="{{ auth()->user()->name }}">
            </div>
            <div class="form-group">
                <label for="email">Email</label><input name="email" class="form-control" placeholder="Email" type="email" value="{{ auth()->user()->email }}">
            </div>
            <div class="checkbox">
            </div><button class="btn btn-primary" type="submit">Update</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->