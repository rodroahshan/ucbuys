<div id="address-edit-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Address Information</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ URL('/user/update_user') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="Telephone">Telephone</label> <input name="telephone" class="form-control" placeholder="Telephone" type="text" value="{{ auth()->user()->telephone }}">
            </div>

            <div class="form-group">
                <label for="city">City</label> <input name="city" class="form-control" placeholder="City" type="text" value="{{ auth()->user()->city }}">
            </div>

            <div class="form-group">
                <label for="state">State</label> <input name="state" class="form-control" placeholder="State" type="text" value="{{ auth()->user()->state }}">
            </div>

            <div class="form-group">
                <label for="country">Country</label> 
                {!! Form::select('country_id', \App\Country::get()->pluck('country_name', 'id'), auth()->user()->country_id, ['placeholder' => 'Select Country', 'class' => 'form-control select2', 'style' => 'width:100%;']) !!}


            </div>

            <div class="form-group">
                <label for="postal_code">Postal Code</label> <input name="postal_code" class="form-control" placeholder="Postal Code" type="text" value="{{ auth()->user()->postal_code }}">
            </div>

            <div class="form-group">
                <label for="address">Address</label> <input name="address" class="form-control" placeholder="Address" type="text" value="{{ auth()->user()->address }}">
            </div>

            <div class="checkbox">
            </div><button class="btn btn-primary" type="submit">Update</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->