{{-- https://devdojo.com/articles/toastr-notifications-in-your-laravel-app --}}

{{-- $notification = array(
    'message' => 'I am a successful message!', 
    'alert-type' => 'success'
);

return Redirect::to('/')->with($notification); --}}

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>