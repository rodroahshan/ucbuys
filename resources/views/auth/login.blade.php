@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">  
        <div class="col-md-6 col-md-offset-2">
            <h2>Sign In</h2>
            <form method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">Email address</label> <input name="email" class="form-control" id="email" placeholder="Email" type="email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label> <input name="password" class="form-control" id="password" placeholder="Password" type="password">
                </div>

                <div class="form-group">
                    <a href="{{ action('AuthController@redirectToProvider') }}">Login With Facebook</a>
                </div>
                <button class="btn btn-default" type="submit">Submit</button>
            </form>
        </div>
    </div><!--/ row -->
</div><!--/ container -->        
@endsection