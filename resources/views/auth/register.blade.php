@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">  
        <div class="col-md-6 col-md-offset-2">
            <h2>Sign Up</h2>
            <div id="register-login">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#buyer" aria-controls="buyer" role="tab" data-toggle="tab">Buyer</a></li>
                <li role="presentation"><a href="#traveler" aria-controls="traveler" role="tab" data-toggle="tab">Traveler</a></li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="buyer">
                    <!-- TRAVELER -->
                    @include('layouts.errors')
                    {!! Form::open(['url' => '/auth/register', 'method' => 'POST']) !!}
                        {{ csrf_field() }}
                        {!! Form::hidden('role', 2) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Your Name') !!}
                            {!! Form::text('name', '', ['placeholder' => 'Your Name', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', '', ['placeholder' => 'Your Email', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Password') !!}
                            {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Confirm Password') !!}
                            {!! Form::password('password_confirmation', ['placeholder' => 'Password Confirmation', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Save" class="btn btn-primary">
                        </div>
                    {!! Form::close() !!}
                </div><!--end of buyer-->
                <div role="tabpanel" class="tab-pane" id="traveler">
                    <!-- TRAVELER -->
                    @include('layouts.errors')
                    {!! Form::open(['url' => '/auth/register', 'method' => 'POST']) !!}
                        {{ csrf_field() }}
                        {!! Form::hidden('role', 3) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Your Name') !!}
                            {!! Form::text('name', '', ['placeholder' => 'Your Name', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', '', ['placeholder' => 'Your Email', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Password') !!}
                            {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Confirm Password') !!}
                            {!! Form::password('password_confirmation', ['placeholder' => 'Password Confirmation', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Save" class="btn btn-primary">
                        </div>
                    {!! Form::close() !!}

                </div><!--end of traveler-->
              </div><!--end of tab-content-->

            </div><!--end of register-login-->
        </div>
    </div><!--/ row -->
</div><!--/ container -->       
@endsection