
<li><a href="{{ url('/how-it-works') }}">How It Works</a></li>
<li><a href="{{ url('/about-us') }}">About Us</a></li>
{{-- <li><a href="{{ url('/faq') }}">Faq</a></li> --}}
{{-- <li><a href="{{ url('/traveler') }}">Become A Traveler</a></li> --}}

@if(auth()->check())
	<li role="presentation" class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
		  {{ auth()->user()->name }} <span class="caret"></span>
		</a>
		<ul class="dropdown-menu">
		   <li><a href="/dashboard">Dashboard</a></li>
		   <li><a href="/auth/logout">Logout</a></li>
		</ul>
	</li>
@else
	<li><a href="/auth/register">Register</a></li>
	<li><a href="/auth/login">Login</a></li>
@endif


<li><a href="{{ url('/contact-us') }}">Contact Us</a></li>


