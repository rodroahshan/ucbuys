{{-- {{ dd(session()->all())}} --}}
<!-- Navigation -->
    <nav class="navbar navbar-fixed-top" role="navigation">
        <div class="container">

            

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><img style="width: 150px;" src="{{ url('/images/logo-1.png')}}">{{-- BD BOOM --}}</a>

                <div class="cart-bx">
                    <a href="{{ url('/products/cart') }}">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span class="cart-icon">{{ \App\Cart::get_grand_qty() }}</span>
                    </a>
                </div><!--/ cart-icon -->

                <div class="search-in-nav">
                    <form action="{{ action('ProductController@search') }}" method="GET">
                        <input name="query" type="text" placeholder="Search What You Want From India" value="{{ request('query') }}">
                    </form>
                </div>
            </div>


            

            

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @include('layouts.nav')
                </ul>
            </div>


            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>