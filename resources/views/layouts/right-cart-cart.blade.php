<div class="right-cart">
	<div class="loc-cart">
		<h6>Location</h6>
		<p>Dhaka Bangladesh</p>
	</div><!--/ loc-cart -->
	<div class="order-sum-cart">
		<h4>Order Summery</h4>
		<ul>
			<li>
				<span class="sum-label">SubTotal <span class="grand_qty">{{ $total_cart['grand_qty'] }}</span> Items</span>
				<span class="sum-value grand_net_price">{{ on_bdt($total_cart['grand_net_price']) }}</span>
			</li>
			<li>
				<span class="sum-label">Traveler Takes</span>
				<span class="sum-value grand_shipping_charge">{{ on_bdt($total_cart['grand_shipping_charge']) }}</span>
			</li>
			<li>
				<span class="sum-label">Bd Boom Takes</span>
				<span class="sum-value grand_bdboom_charge">{{ on_bdt($total_cart['grand_bdboom_charge']) }}</span>
			</li>
			<li>
				<span class="sum-label">Delivery Charge</span>
				<span class="sum-value delivery_charge">{{ on_bdt($total_cart['delivery_charge']) }}</span>
			</li>
			
			<li>
				<span class="sum-label">Grand Total</span>
				<span class="sum-value grand_total">{{ on_bdt($total_cart['grand_total']) }}</span>
			</li>

			<li>
				<span class="sum-label">Discount (-)</span>
				<span class="sum-value discount">{{ on_bdt($total_cart['discount']) }}</span>
			</li>
		</ul>
	</div><!--/ order-sum-cart -->
	<div class="order-voucher-cart">
		<form action="">
			<input type="text" placeholder="Enter Voucher Code">
			<input type="submit" value="Submit">
		</form>
	</div><!--/ order-voucher-cart -->
	<div class="order-total-cart">
		<ul>
			<li>
				<div class="order-total-cart-label">Total</div>
				<div class="order-total-cart-value all_total">{{ on_bdt($total_cart['all_total']) }}</div>
			</li>
			<li>
				<div class="order-total-cart-label">Gateway Fee</div>
				<div class="order-total-cart-value gateway_charge">{{ on_bdt($total_cart['gateway_charge']) }}</div>
			</li>
			<li>
				<div class="order-total-cart-label">Master Total</div>
				<div class="order-total-cart-value master_total">{{ on_bdt($total_cart['master_total']) }}</div>
			</li>
		</ul>
		
	</div><!--/ order-total-cart -->
	
</div><!--/ right-cart -->