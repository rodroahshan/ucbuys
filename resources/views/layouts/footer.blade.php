<footer class="footer-area footer-bg">
    <div class="footer-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6">
                    <div class="footer-widget about">
                        <div class="widget-title">
                            <div class="footer-logo">
                                <a href="index.html"><img src="{{ url('/images/logo-1.png')}}" alt="footer logo"></a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <p>
                                We are here to buy Indian amazon products from Bangladesh by bdboom.com. You will get any amazon.in products in our site.
                            </p>
                            <ul class="footer-social">
                                <li><a target="_blank" href="https://www.facebook.com/Bd-Boom-361982474397468/" class="facebook"> <i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a target="_blank" href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a target="_blank" href="#" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a target="_blank" href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li><a target="_blank" href="#" class="skype"><i class="fa fa-skype"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-xl-2 col-lg-2 col-md-6">
                    <div class="footer-widget">
                        <div class="widget-title">
                            <h3 class="title">Useful Links</h3>
                        </div>
                        <div class="widget-body">
                            <ul class="footer-links">
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a href="{{ url('/about-us') }}">About Us</a></li>
                                <li><a href="{{ url('/how-it-works') }}">How it works</a></li>
                                <li><a href="{{ url('/why-us') }}">Why Us</a> </li>
                                <li><a href="{{ url('/contact-us') }}">Contact Us </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-lg-5 col-md-6">
                    <div class="footer-widget contact-info">
                        <div class="widget-title">
                            <h3 class="title">Get in touch</h3>
                        </div>
                        <div class="widget-body">
                            <p>56, East Tejturi Bazar, Rahman Mansion (2nd Floor), Near ​Farmgate Over, Bridge, Dhaka 1215</p>
                            <p>Mobile: +88 0191 44 88 363, 01914 22 12 43, 016 80 81 63 43,</p>
                            <p>E-mail: <a href="#">support@bdboom.com</a></p>
                            <p>Website: <a href="#">www.bdboom.com</a></p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="copyright-area">
            Copyright &copy; Bdboom 2018
        </div>
    </div>
    
    <!--footer top end-->
    
</footer>

@include('modals.login')
    

    <script type="text/javascript">
        window.base_url = <?php echo json_encode(url('/')); ?>;
    </script>

    <?php $settings = \App\Http\Controllers\HelperController::$front_settings; ?>
    @foreach($settings['js'] as $js)
        <script src="{{ asset($js) }}"></script>
    @endforeach

