<div class="right-cart">
	<div class="loc-cart">
		<h6>Location</h6>
		<p>Dhaka Bangladesh</p>
	</div><!--/ loc-cart -->
	<div class="order-sum-cart">
		<h4>Order Summery</h4>
		<ul>
			<li>
				<span class="sum-label">SubTotal <span class="sub-qty">{{ $cart_session->net_qty }}</span> Items</span>
				<span class="sum-value sub-tot-crt">{{ on_bdt($cart_session->sub_total) }}</span>
			</li>
			<li>
				<span class="sum-label">Shipping Fee</span>
				<span class="sum-value shipping-crt">{{ on_bdt($cart_session->shipping) }}</span>
			</li>
		</ul>
	</div><!--/ order-sum-cart -->
	<div class="order-voucher-cart">
		<form action="">
			<input type="text" placeholder="Enter Voucher Code">
			<input type="submit" value="Submit">
		</form>
	</div><!--/ order-voucher-cart -->
	<div class="order-total-cart">
		<div class="order-total-cart-label">Total</div>
		<div class="order-total-cart-value">{{ on_bdt($cart_session->grand_total) }}</div>
	</div><!--/ order-total-cart -->
	<div class="order-checkout-cart">
        @if(auth()->check())
            <a class="btn btn-default" href="{{ action('ProductController@order') }}">Place Order</a>
        @else
			<a class="btn btn-default" href="#" data-toggle="modal" data-target="#signin">Place Order</a>
        @endif

	</div><!--/ order-checkout-cart -->
</div><!--/ right-cart -->