<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Buy Amazon products from BD @yield('title')</title>

    <?php $settings = \App\Http\Controllers\HelperController::$front_settings; ?>
    @foreach($settings['css'] as $css)
        <link rel="stylesheet" href="{{ asset($css) }}">
    @endforeach

    @yield('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="body-padding-1">
   
   <?php //$cart_session = new \App\Cart; ?>

    @include('layouts.header')
    <!-- Page Content -->
            @include('layouts.errors')
            
            @yield('content')

            {{-- @include('layouts.sidebar') --}}
        <hr>
        @include('layouts.footer')
        @yield('footer')
        @yield('js')
    
    <script>
        toastr.options.timeOut = 100;
        toastr.options.extendedTimeOut = 0;

        // toastr.success("Working");
    </script>
    @include('tools.toaster')

</body>

</html>
