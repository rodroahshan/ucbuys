@extends('dashboard.layouts.layout')
@section('dash-con')
	<div class="my-travel-products-dsbrd">
		<div class="col-md-6">
			<div class="available-items tr-item-bx">
				<h3>Available Item</h3>
				<div class="tr-item-list">
					<ul>
						@foreach($products as $product)
						<li class="av-item" product-id="{{ $product->id }}">
							<img src="{{ $product->product_image }}" alt="">
							<p>{{ $product->title }}</p>
							<div class="overlay-act">
								<p><i class="fa fa-plus-circle" aria-hidden="true"></i></p>
							</div><!--/ overlay-act -->
						</li>
						@endforeach
					</ul>
				</div><!--/ tr-item-list -->
			</div><!--/ available-items -->
		</div><!--/ col-md-6 -->

		<div class="col-md-6">
			<div class="my-items tr-item-bx">
				<h3>I will Carry</h3>
				<div class="tr-item-list">
				</div><!--/ tr-item-list -->
			</div><!--/ available-items -->
		</div><!--/ col-md-6 -->
	</div><!--/  -->
@endsection