@extends('dashboard.layouts.layout')
@section('dash-con')
	@include('layouts.errors')
	    {!! Form::open(['url' => '/dashboard/my_travels/create', 'method' => 'POST']) !!}
	        {{ csrf_field() }}
	        {!! Form::hidden('role', 2) !!}
	        <div class="form-group">
	            {!! Form::label('country_from', 'Country From') !!}
	            {!! Form::select('country_from', \App\Country::pluck('country_name', 'id'), null, ['placeholder' => 'Country From', 'class' => 'form-control select2']) !!}
	        </div>
	        <div class="form-group">
	            {!! Form::label('country_to', 'Country To') !!}
	            {!! Form::select('country_to', \App\Country::pluck('country_name', 'id'), null, ['placeholder' => 'Country To', 'class' => 'form-control select2']) !!}
	        </div>
	        <div class="form-group">
	            {!! Form::label('luckege_qty', 'Luckege Quantity') !!}
	            {!! Form::select('luckege_qty', ['1' => 1, '2' => 2], null, ['placeholder' => 'Select One', 'class' => 'form-control']) !!}
	        </div>
	        <div class="form-group">
	            {!! Form::label('travel_date', 'Travel Date') !!}
	            {!! Form::date('travel_date', \Carbon\Carbon::now(), ['class' => 'form-control']); !!}
	        </div>
	        <div class="form-group">
	            <input type="submit" value="Save" class="btn btn-primary">
	        </div>
	    {!! Form::close() !!}
@endsection