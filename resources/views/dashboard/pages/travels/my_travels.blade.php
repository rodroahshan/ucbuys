@extends('dashboard.layouts.layout')
@section('dash-con')
	<div class="my-travel-dsbrd">
		{{-- {{ dd($travels) }} --}}
		@if(count($travels))
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Country From</th>
						<th>Country To</th>
						<th>Luckege Qty</th>
						<th>Travel Date</th>
						<th>Items</th>
					</tr>
				</thead>
				<tbody>
					@foreach($travels as $travel)
						<tr>
							<td>#{{ $travel->id }}</td>
							<td>{{ $travel->cntry_fr->country_name }}</td>
							<td>{{ $travel->cntry_to->country_name }}</td>
							<td>{{ $travel->luckege_qty }}</td>
							<td>{{ formated_date($travel->travel_date) }}</td>
							<td><a href="{{ url('/dashboard/my_travel_details', $travel->id )}}">View Items</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>	
			{{ $travels->links() }}
		@else
			<div class="center">
			 	<h4>No Data</h4>
				<p><a class="btn btn-default" href="{{ action('TravelController@create') }}">Add Travel</a></p>
			</div><!--/ center -->
		@endif			
	</div><!--/  -->
@endsection