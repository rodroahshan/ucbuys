@extends('dashboard.layouts.layout')
@section('dash-con')
	<div class="account-d-head">
		<h4>{{ auth()->user()->name }}</h4>
		<div class="acc-cst-id">
			<p>Customer ID</p>
			<h4>{{ auth()->id() }}</h4>
		</div>
	</div><!--end of account-d-head -->
@endsection