@extends('dashboard.layouts.layout')
@section('dash-con')
	<div class="box-account-content">
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-title">
						<h3>Contact Information</h3><a class="link-edit" href="#" data-toggle="modal" data-target="#contact-edit-modal"><span><span>Edit</span></span></a>
					</div>
					<div class="box-content">
						<p>{{ auth()->user()->name }}<br>
						{{ auth()->user()->email }}<br></p>
						<p class="change-pass"><a class="button-link btn-link-01" href="#" data-toggle="modal" data-target="#update-password-modal">Change Password</a></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box">
					<div class="box-title">
						<h3>Newsletters</h3><a class="link-edit" href="https://www.pickaboo.com/newsletter/manage/"><span><span>Edit</span></span></a>
					</div>
					<div class="box-content">
						<p>You are currently not subscribed to any newsletter.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="box">
				<div class="box-title col-md-12">
					<h3 class="address-book">Address Book</h3>
				</div>
				<div class="box-content">
					<div class="col-md-6">
						<h4>Default Billing Address</h4><a class="link-edit" data-toggle="modal" data-target="#address-edit-modal" href="#"><span><span>Edit Address</span></span></a>
						<address>
							{{ auth()->user()->name }}<br>
							{{ auth()->user()->address }}<br>
							{{ auth()->user()->city }}, {{ auth()->user()->state }},<br>
							{{ auth()->user()->country }}<br>
							T: {{ auth()->user()->telephone }}<br>
						</address>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@include('modals.edit-contact')
	@include('modals.edit-address')
	@include('modals.update-password')
@endsection