@extends('dashboard.layouts.layout')
@section('dash-con')
	
	<div class="my-order-dsbrd">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Order</th>
					<th>Date</th>
					<th>Ship To</th>
					<th>Order Total</th>
					<th>Delivery Status</th>
					<th>Payment</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($orders as $order)
					<tr>
						<td>#{{ $order->id }}</td>
						<td>{{ $order->created_at->toFormattedDateString() }}</td>
						<td>{{ $order->user->name }}</td>
						<td>{{ on_bdt($order->master_total) }}</td>
						<td>{{ \App\Order::get_delivery_status_name_by_key($order->delivery_status) }}</td>
						<td>
							@if($order->payment_status != 1)
								<a href="{{ action('OrderController@pay_now', $order->id) }}">Pay Now</a>
							@else
								{{ \App\Order::get_payment_status_name_by_key($order->payment_status) }}
							@endif
						</td>
						<td><a href="{{ url('/dashboard/my_order_details', $order->id )}}">View Order</a></td>
					</tr>
				@endforeach
			</tbody>
		</table>	

		{{ $orders->links() }}
	</div><!--/  -->
@endsection