@extends('dashboard.layouts.layout')
@section('dash-con')
	<div class="my-order-products-dsbrd">

		<div class="container-fluid">
		  
		  <ul class="list-unstyled multi-steps">
		  	<?php $current_status = $order->delivery_status; ?>
			@foreach(\App\Order::$delivery_status as $key => $status)
				<?php $class = $current_status == (int) $key ? 'is-active' : ''; ?>
				<li class="{{$class}}">{{$status}}</li>
			@endforeach
		  </ul>
		</div>
		
		<br />


		<table class="table table-bordered my_order-info">
			<thead>
				<tr>
					<td colspan="2"><i class="fa fa-briefcase" aria-hidden="true"></i> Order Details</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="width: 50%;">
						Order ID: <b>#{{ $order->id }}</b><br>
						Order Date: <b>{{ $order->created_at->toFormattedDateString() }}</b><br />
						Payment Status: <b>{{ \App\Order::get_payment_status_name_by_key($order->payment_status) }}</b>
					</td>
					<td>Payment Method: <b>@if($order->payment){{ $order->payment->trans_type }}@endif</b></td>
				</tr>
			</tbody>
		</table>

		<table class="table table-bordered my_delivery-info">
			<thead>
				<tr>
					<td colspan="2"><i class="fa fa-briefcase" aria-hidden="true"></i> Delivery Details</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="width: 50%;">
						City: <b>{{ $order->shipping_city }}</b><br>
						State: <b>{{ $order->shipping_state }}</b>
					</td>
					<td>
						Address: <b>{{ $order->shipping_address }}</b><br>
						Phone: <b>{{ $order->shipping_telephone }}</b>
					</td>
				</tr>
			</tbody>
		</table>

		<table class="table table-bordered my_order-items">
			<thead>
				<tr>
					<td>Product Name</td>
					<td>Image</td>
					<td class="text-right">Quantity</td>
					<td class="text-right">Price</td>
					<td class="text-right">Total</td>
				</tr>
			</thead>
			<tbody>
				@foreach($order->products as $product)
					<tr>
						<td>
							<a href="{{ url('/products', $product->product_id )}}" target="_blank" title="{{ $product->title }}">{{ $product->title }}</a><br>
							{{-- &nbsp;- <b>Status</b>: <b>Shipped from CN Warehouse</b> --}}
						</td>
						<td><img style="width:30px;" src="{{ $product->product_image }}"></td>
						<td class="text-center"><span>{{ $product->qty }}</span></td>
						<td class="text-right"><span>{{ on_bdt($product->total) }}</span></td>
						<td class="text-right"><span>{{ on_bdt($product->master_total) }}</span></td>
					</tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td class="text-right" colspan="4"><span>Product Total</span></td>
					<td class="text-right"><b>{{ on_bdt($order->grand_total) }}</b></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4">Delivery Fee</td>
					<td class="text-right"><b>{{ on_bdt($order->delivery_charge) }}</b></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4">Discount(-)</td>
					<td class="text-right"><b>{{ on_bdt($order->discount) }}</b></td>
				</tr>
				<tr>
					<td class="text-right" colspan="4">Gateway Charge</td>
					<td class="text-right"><b>{{ on_bdt($order->gateway_charge) }}</b></td>
				</tr>
				{{-- <tr>
					<td class="text-right" colspan="4"><span>Free Shipping</span></td>
					<td class="text-right"><b>tk.0</b></td>
				</tr> --}}
				<tr class="total-big">
					<td class="text-right" colspan="4"><span>To be Paid</span></td>
					<td class="text-right"><b>{{ on_bdt($order->master_total) }}</b></td>
				</tr>
				<tr class="total-big">
					<td class="text-right" colspan="4"><span>Paid</span></td>
					<td class="text-right"><b>@if($order->payment){{ on_bdt($order->payment->trans_amount) }}@endif</b></td>
				</tr>
			</tfoot>
		</table>
	</div><!--/  -->
@endsection