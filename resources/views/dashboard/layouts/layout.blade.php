@extends('layouts.layout')
@section('content')
	<div class="container" style="margin-top:50px;">
		<div class="col-md-3">
			<div class="dashboard-nav">
				@include('dashboard.nav.nav')
			</div><!--end of dashboard-nav -->
		</div><!--/ col-md-4 -->
		<div class="col-md-9">
			<div class="dashboard-content">
				@yield('dash-con')
			</div><!--end of dashboard-content -->
		</div><!--end of col-md-8 -->
	</div><!--/ container -->
@endsection