<ul class="nav nav-pills nav-stacked">
  <li{{ Request::is('dashboard') ? ' class=active' : null }}><a href="{{ url('/dashboard') }}">Account Dashboard</a></li>
  <li{{ Request::is('dashboard/account') ? ' class=active' : null }}><a href="{{ url('/dashboard/account') }}">Account Information</a></li>
  
  @if(auth()->user()->role == 2)
  	@include('dashboard.nav.buyer')
  @endif

  @if(auth()->user()->role == 3)
  	@include('dashboard.nav.traveler')
  @endif
  <li><a href="{{ url('/dashboard/wallet')}}">Wallet</a></li>
  <li><a href="{{ url('/auth/logout')}}">Logout</a></li>
</ul>