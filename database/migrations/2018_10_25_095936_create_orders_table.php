<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void     
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('payment_id')->nullable();

            $table->integer('grand_qty');
            $table->double('grand_net_price', 10, 2)->default(0);
            $table->double('grand_shipping_charge', 10, 2)->default(0);
            $table->double('grand_bdboom_charge', 10, 2)->default(0);
            $table->double('grand_total', 10, 2)->default(0);
            $table->double('discount', 10, 2)->default(0);
            $table->double('delivery_charge', 10, 2)->default(0);
            $table->double('all_total', 10, 2)->default(0);
            $table->double('gateway_charge', 10, 2)->default(0);
            $table->double('master_total', 10, 2)->default(0);

            $table->string('shipping_telephone')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_state')->nullable();
            $table->string('shipping_postal_code')->nullable();
            $table->text('shipping_address')->nullable();
            $table->integer('shipping_country_id')->nullable();

            $table->tinyInteger('is_refund')->nullable();

            $table->integer('payment_status')->nullable()->comment('1:Paid, 2:Pending, 3:Cancelled, 4:Failed');

            $table->integer('delivery_status')->default(0)->comment("   
                                                                    '0' => 'Payment Pending',
                                                                    '1' => 'Payment Confirmed',
                                                                    '2' => 'Processing',
                                                                    '3' => 'Shipped from India',
                                                                    '4' => 'Arrived in BD',
                                                                    '5' => 'Deliver Processed',
                                                                    '6' => 'Delivered',
                                                                    ");
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
