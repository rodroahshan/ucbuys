<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gateway_type')->comment('1:WalletMix');
            $table->integer('order_id')->nullable();
            $table->string('trans_token');
            $table->double('trans_amount_without_charge', 10, 2);
            $table->double('trans_amount', 10, 2);
            $table->double('trans_charge', 10, 2);
            $table->double('trans_discount', 10, 2);
            $table->string('trans_type');
            $table->string('trans_ip');
            $table->string('trans_message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
