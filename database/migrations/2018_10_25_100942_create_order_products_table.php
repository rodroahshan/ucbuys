<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id');
            $table->string('product_id')->nullable();
            $table->integer('product_src')->nullable()->comment('0: Other, 1:Amazon, 2:Flipcart');

            $table->string('title')->nullable();
            $table->string('product_image')->nullable();
            $table->integer('qty')->nullable();
            $table->double('price', 10, 2)->nullable();
            $table->double('offer_price', 10, 2)->nullable();
            $table->double('net_price', 10, 2)->nullable();
            $table->double('shipping_charge', 10, 2)->nullable();
            $table->double('bdboom_charge', 10, 2)->nullable();
            $table->double('total', 10, 2)->nullable();
            $table->double('master_total', 10, 2)->nullable();

            $table->text('affiliates_url')->nullable();
            $table->text('detail_page_url')->nullable();
            $table->tinyInteger('is_refund')->nullable();

            $table->string('note')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
