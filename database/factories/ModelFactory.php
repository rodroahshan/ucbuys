<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'role' => 2,
        'email' => $faker->unique()->safeEmail,    
        'password' => md5(1234),
        'telephone' => $faker->phoneNumber,  
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->state,
        'country_id' => \App\Country::all()->random()->id,
        'postal_code' => $faker->postcode,
        'remember_token' => str_random(10),   
    ];
});


$factory->define(App\Post::class, function (Faker\Generator $faker){
	return [
		'user_id' => factory('App\User')->create()->id,
		'title' => $faker->sentence,
		'body' => $faker->paragraph,
	];
});