<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        // create 2 posts
    	$post2 = factory('App\Post')->create(['created_at' => \Carbon\Carbon::now()->subMonth()]);
        $post1 = factory('App\Post')->create();
        // fetch posts

        $archives = \App\Post::archives()->toArray();

        //dd($post1->created_at->format('F'));
        //dd($archives);
        $this->assertEquals([
          [
            "year" => $post2->created_at->format('Y'),
            "month" => $post2->created_at->format('F'),
            "published" => 1,
          ],
          [
            "year" => $post1->created_at->format('Y'),
            "month" => $post1->created_at->format('F'),
            "published" => 1,
          ],
        ], $archives);

        
        //return $archives;

        // compare the posts


        //$this->assertCount(2, $archives);
    }
}
